package com.weaseldiesel.core.reflection;

import java.io.Serializable;

import com.weaseldiesel.core.utility.Keyword; 

public class CodeFile implements Serializable
{
	private static final long serialVersionUID = 3L;
	
	private String originalCodeFilePath;
	private String modifiedCodeFilePath;
	private int lineCount;
	private String fingerprintPath;
	
	//Constructors
	
	public CodeFile() 
	{
		super();
		this.originalCodeFilePath = null;
		this.modifiedCodeFilePath = null;
		this.lineCount = -1;
		this.fingerprintPath = null;
	}
	
	public CodeFile(String codeFilePath, int lineCount,
			String fingerprintPath) 
	{
		super();
		this.originalCodeFilePath = codeFilePath;
		this.modifiedCodeFilePath = findModifiedCodeFilePath(originalCodeFilePath);
		this.lineCount = lineCount;
		this.fingerprintPath = fingerprintPath;
	}
	
	private static String findModifiedCodeFilePath(String path)
	{
		String modifiedCodeFilePath = path.replaceFirst(Keyword.ORIGINAL_CODE_FILES, Keyword.MODIFIED_CODE_FILES);
		modifiedCodeFilePath = modifiedCodeFilePath.substring(0, modifiedCodeFilePath.length() - 5) + Keyword.TEXT_EXTENSION;
		return modifiedCodeFilePath;
	}
	
	//Getter & Setter

	public int getLineCount() {
		return lineCount;
	}

	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

	public String getFingerprintPath() {
		return fingerprintPath;
	}

	public void setFingerprintPath(String fingerprintPath) {
		this.fingerprintPath = fingerprintPath;
	}

	public String getOriginalCodeFilePath() {
		return originalCodeFilePath;
	}

	public void setOriginalCodeFilePath(String originalCodeFilePath) {
		this.originalCodeFilePath = originalCodeFilePath;
	}

	public String getModifiedCodeFilePath() {
		return modifiedCodeFilePath;
	}

	public void setModifiedCodeFilePath(String modifiedCodeFilePath) {
		this.modifiedCodeFilePath = modifiedCodeFilePath;
	}
}
