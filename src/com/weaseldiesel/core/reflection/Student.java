package com.weaseldiesel.core.reflection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.utility.Keyword;


public class Student implements Serializable 
{
	private static final long serialVersionUID = 2L;
	
	private String studentID;
	private String studentPath;
	private List<CodeFile> codeFiles;
	private String signaturePath;
	private String matchesPath;
	private List<String> matches;
	private String errorLogPath;
	
	//Constructors
	
	public Student() 
	{
		super();
		this.studentPath = null;
		this.signaturePath = null;
		this.codeFiles = new ArrayList<CodeFile>();
		this.matchesPath = null;
		this.errorLogPath = null;
	}

	public Student(String studentPath) 
	{
		super();
		this.studentID = studentPath.substring(studentPath.lastIndexOf("\\") + 1);
		this.studentPath = studentPath;
		this.signaturePath = studentPath + "\\" + Keyword.SIGNATURE + "\\" + Keyword.SIGNATURE + 
								Keyword.SERIALIZATION_EXTENSION;
		this.codeFiles = new ArrayList<CodeFile>();
		this.matchesPath = studentPath + "\\" + Keyword.MATCHES;
		this.errorLogPath = studentPath + "\\" + Keyword.ERROR_LOG + Keyword.SERIALIZATION_EXTENSION;
	}
	
	public Student(String studentPath, List<CodeFile> codeFiles) 
	{
		super();
		this.studentID = studentPath.substring(studentPath.lastIndexOf("\\") + 1);
		this.studentPath = studentPath;
		this.signaturePath = studentPath + "\\" + Keyword.SIGNATURE + "\\" + 
					Keyword.SIGNATURE + Keyword.SERIALIZATION_EXTENSION;
		this.codeFiles = codeFiles;
		this.matchesPath = studentPath + "\\" + Keyword.MATCHES;
		this.errorLogPath = studentPath + "\\" + Keyword.ERROR_LOG + Keyword.SERIALIZATION_EXTENSION;
	}
	
	
	//Add code file methods
	
	public CodeFile addCodeFile(String codeFilePath, int lineCount, String fingerprintPath)
	{
		CodeFile c = new CodeFile(codeFilePath, lineCount, fingerprintPath);
		codeFiles.add(c);
		
		return c;
	}
	
	public CodeFile addCodeFile(String codeFilePath, int lineCount)
	{
		int index = codeFilePath.indexOf(Keyword.ORIGINAL_CODE_FILES);
		int index2 = index + Keyword.ORIGINAL_CODE_FILES.length();
		
		String fingerprintPath = codeFilePath.substring(0, index) + 
							Keyword.FINGERPRINTS + 
							codeFilePath.substring(index2);
		
		CodeFile c = new CodeFile(codeFilePath, lineCount, fingerprintPath);
		codeFiles.add(c);
		
		return c;
	}
	
	public CodeFile addCodeFile(String codeFilePath)
	{
		int index = codeFilePath.indexOf(Keyword.ORIGINAL_CODE_FILES);
		int index2 = index + Keyword.ORIGINAL_CODE_FILES.length();
		
		String fingerprintPath = codeFilePath.substring(0, index) + 
							Keyword.FINGERPRINTS + 
							codeFilePath.substring(index2);
		
		CodeFile c = new CodeFile(codeFilePath, -1, fingerprintPath);
		codeFiles.add(c);
		
		return c;
	}
	
	public List<CodeFile> addCodeFile(List<String> codeFiles)
	{
		for(String codeFilePath : codeFiles)
		{
			int index = codeFilePath.indexOf(Keyword.ORIGINAL_CODE_FILES);
			int index2 = index + Keyword.ORIGINAL_CODE_FILES.length();
			
			String fingerprintPath = codeFilePath.substring(0, index) + 
								Keyword.FINGERPRINTS + 
								codeFilePath.substring(index2, codeFilePath.length()-5) + Keyword.SERIALIZATION_EXTENSION;
			
			CodeFile c = new CodeFile(codeFilePath, -1, fingerprintPath);
			this.codeFiles.add(c);
		}
		
		return this.codeFiles;
	}

	public boolean hasMatches()
	{
		return getMatchCount() > 0;
	}
	
	public int getMatchCount()
	{
		return this.getMatches().size();
	}
	
	//Getter & Setter
	
	public String getStudentID() {
		return studentID;
	}
	
	public void setStudentID(String studentID) {
		this.studentID = studentID;
	}
	
	public String getStudentPath() {
		return studentPath;
	}

	public void setStudentPath(String studentPath) {
		this.studentPath = studentPath;
	}

	public List<CodeFile> getCodeFiles() {
		return codeFiles;
	}

	public void setCodeFiles(List<CodeFile> codeFiles) {
		this.codeFiles = codeFiles;
	}

	public String getSignaturePath() {
		return signaturePath;
	}

	public void setSignaturePath(String signaturePath) {
		this.signaturePath = signaturePath;
	}

	public List<String> getMatches() {
		return matches;
	}

	public void setMatches(List<String> matches) {
		this.matches = matches;
	}

	public String getMatchesPath() {
		return matchesPath;
	}

	public void setMatchesPath(String matchesPath) {
		this.matchesPath = matchesPath;
	}

	public String getErrorLogPath() {
		return errorLogPath;
	}

	public void setErrorLogPath(String errorLogPath) {
		this.errorLogPath = errorLogPath;
	}
}
