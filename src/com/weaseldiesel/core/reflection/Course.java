package com.weaseldiesel.core.reflection;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.datastructure.ObjectSerializer;
import com.weaseldiesel.core.utility.Keyword;


public class Course extends ObjectSerializer implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String coursePath;
	private List<Student> students;
	private float courseCopyRatio;
	
	//Constructors
	
	public Course() 
	{
		super();
		this.coursePath = null;
		this.students = new ArrayList<Student>();
	}
	
	public Course(String coursePath) 
	{
		super();
		this.coursePath = coursePath;
		this.students = new ArrayList<Student>();
	}
	
	public Course(String coursePath, List<String> studentIds) 
	{
		super();
		this.coursePath = coursePath;
		this.students = new ArrayList<Student>();
		for(String st : studentIds)
			this.students.add(new Student(coursePath + "\\" + st));
	}

	public boolean serialize()
	{
		File file = new File(coursePath + "\\" + Keyword.REFLECTION + Keyword.SERIALIZATION_EXTENSION);
		return serializeObject(this, file);
	}
	
	//Add student methods
	
	public Student addStudent(Student st)
	{
		this.students.add(st);
		return st;
	}

	public Student addStudent(String studentName)
	{
		Student student = new Student(coursePath + "\\" + studentName);
		this.students.add(student);
		return student;
	}
	
	//Getter & Setter
	
	public String getCoursePath() 
	{
		return coursePath;
	}

	public void setCoursePath(String coursePath) 
	{
		this.coursePath = coursePath;
	}

	public List<Student> getStudents() 
	{
		return students;
	}

	public void setStudents(List<Student> students) 
	{
		this.students = students;
	}

	public float getCourseCopyRatio() 
	{
		return courseCopyRatio;
	}

	public void setCoursecopyRatio(float courseCopyRatio) 
	{
		this.courseCopyRatio = courseCopyRatio;
	}	
}
