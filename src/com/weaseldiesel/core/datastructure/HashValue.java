package com.weaseldiesel.core.datastructure;

import java.io.Serializable;

public class HashValue implements Serializable
{
	private static final long serialVersionUID = 9L;
	
	private long hashValue;
	private int startPosition;
	private int endPosition;
	
	public HashValue() 
	{
		super();
	}
	
	public HashValue(long hashValue, int startPosition, int endPosition) 
	{
		super();
		this.hashValue = hashValue;
		this.startPosition = startPosition;
		this.endPosition = endPosition;
	}
	

	public String toString()
	{
		return "[" + this.getStartPosition() + ", " + this.getEndPosition() + ", " + this.hashValue + "]";
	}

	
	//Getter & Setter
	public long getHashValue() 
	{
		return hashValue;
	}

	public void setHashValue(long hashValue) 
	{
		this.hashValue = hashValue;
	}

	public int getStartPosition() 
	{
		return startPosition;
	}

	public void setStartPosition(int startPosition) 
	{
		this.startPosition = startPosition;
	}

	public int getEndPosition() 
	{
		return endPosition;
	}

	public void setEndPosition(int endPosition) 
	{
		this.endPosition = endPosition;
	}
}
