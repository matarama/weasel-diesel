package com.weaseldiesel.core.datastructure;

import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.algorithm.RabinKarpRollingHash;


public class Window 
{
	public static final int INT_MAX = RabinKarpRollingHash.PRIME_MOD -1;
	
	private int t = 30; //guarantees to find matches at least t-gram
	private int k = 20; //ignores matches below k-gram
	private int w = t - k + 1; //window size
	private double d = 2/(w+1); //number of finger prints selected
	
	private long arr[];
	private int minValueIndex;
	private int windowNumber = 0;
	
	/**
	 * Calculates window size, density and handles initialization.
	 * 
	 * @param t : length of the matches that guaranteed to be found.
	 * 
	 * @param k : matches that have shorter length than k will be ignored. 
	 */
	public Window(int t, int k)
	{
		this.t = t;
		this.k = k;
		this.w = t - k + 1;
		this.d = 2/((double)w+1);
		minValueIndex = 0;
		arr = new long[w];
		for(int i=0; i<arr.length; i++)
			arr[i] = INT_MAX;
	}
	
	/**
	 * Adds the given hash value to window.
	 * Removes first hash value.
	 * Chooses minimum hash value index from the new window.
	 * 
	 * @param item : (long) item to be added to window.
	 */
	public void addItem(long item)
	{
		//printWindow(arr);
		for(int i=1; i<arr.length; i++)
			arr[i-1] = arr[i];
		--minValueIndex;
		
		arr[arr.length-1] = item;
		
		if(minValueIndex <= 0)
			chooseMinValue();
		else if(arr[minValueIndex] >= item)
			minValueIndex = arr.length-1;
		
		++windowNumber;
	}
	
	/**
	 * Chooses the minimum values from the values contained in window's current state.
	 */
	private void chooseMinValue()
	{
		minValueIndex = 0;
		for(int i=1; i<arr.length; i++)
		{
			if(arr[minValueIndex] >= arr[i])
				minValueIndex = i;
		}
	}
	
	/**
	 * Returns a list containing smallest hash values window base from given parameter.
	 * 
	 * @param hashValues : all hash values calculated for a file.
	 * 
	 * @return (List<HashValue>) hashValues chosen to generate file's fingerprint. 
	 */
	public List<HashValue> getMinimumValuesPerWindow(List<HashValue> hashValues)
	{
		List<HashValue> minimumHashValues = new ArrayList<HashValue>();

		//create first window
		for(int i=0; i<arr.length && i < hashValues.size(); i++)
			arr[i] = hashValues.get(i).getHashValue();
		chooseMinValue();
		//add first windows smallest hash value
		minimumHashValues.add(hashValues.get(windowNumber + minValueIndex));
		
		for(int i=arr.length; i<hashValues.size(); i++)
		{
			addItem(hashValues.get(i).getHashValue());
			minimumHashValues.add(hashValues.get(windowNumber + minValueIndex));
		}
	
		return minimumHashValues;
	}

	/**
	 * Prints the current state of window.
	 * 
	 * Presentation/Observation purposes only.
	 * 
	 * @param arr
	 */
	public void printWindow(long[] arr)
	{
		System.out.print("<<");
		for(int i=0; i<arr.length; i++)
			System.out.print(arr[i] + ",");
		System.out.println(">>");
	}
	
	//Getter & Setter
	
	public int getW() {
		return w;
	}

	public void setW(int w) {
		this.w = w;
	}

	public double getD() {
		return d;
	}

	public void setD(double d) {
		this.d = d;
	}

	public long[] getArr() {
		return arr;
	}

	public void setArr(long[] arr) {
		this.arr = arr;
	}

	public int getMinValueIndex() {
		return minValueIndex;
	}

	public void setMinValueIndex(int minValueIndex) {
		this.minValueIndex = minValueIndex;
	}

	public int getT() {
		return t;
	}

	public void setT(int t) {
		this.t = t;
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}
}
