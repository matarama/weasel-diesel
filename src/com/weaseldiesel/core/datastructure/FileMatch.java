package com.weaseldiesel.core.datastructure;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FileMatch implements Serializable
{
	private static final long serialVersionUID = 5L;
	
	private String file1ID;
	private String file2ID;
	private String file1Path;
	private String file2Path;
	private float normalizedFileSimilarityRatio;
	private float file1SimilarityRatio;
	private float file2SimilarityRatio;
	private int file1TotalLineCount;
	private int file2TotalLineCount;
	private int file1CodeLineCount;
	private int file2CodeLineCount;
	private int file1SimilarLineCount;
	private int file2SimilarLineCount;
	private List<CodeBlockMatch> codeBlockMatches;
	
	// Constructors
	// ---------------------------------------------------------------------------------------
	
	public FileMatch() 
	{
		super();
		this.file1ID = null;
		this.file2ID = null;
		this.file1Path = null;
		this.file2Path = null;
		this.file1SimilarityRatio = 0;
		this.file2SimilarityRatio = 0;
		this.file1TotalLineCount = 0;
		this.file2TotalLineCount = 0;
		this.file1SimilarLineCount = 0;
		this.file2SimilarLineCount = 0;
		this.codeBlockMatches = null;
	}

	public FileMatch(String file1Path, String file2Path, 
			int file1TotalLineCount, int file2TotalLineCount,
			int file1CodeLineCount, int file2CodeLineCount) 
	{
		super();
		this.file1ID = file1Path.substring(file1Path.lastIndexOf('\\') + 1);
		this.file2ID = file2Path.substring(file2Path.lastIndexOf('\\') + 1);
		this.file1Path = file1Path;
		this.file2Path = file2Path;
		this.file1TotalLineCount = file1TotalLineCount;
		this.file2TotalLineCount = file2TotalLineCount;
		this.file1CodeLineCount = file1CodeLineCount;
		this.file2CodeLineCount = file2CodeLineCount;
		this.codeBlockMatches = new ArrayList<CodeBlockMatch>();
	}

	// Methods
	// ---------------------------------------------------------------------------------------
	
	public void addCodeBlockMatch(CodeBlockMatch codeBlockMatch)
	{
		codeBlockMatches.add(codeBlockMatch);
	}
	
	public void addCodeBlockMatch(Line firstFileLines, Line secondFileLines)
	{
		codeBlockMatches.add(new CodeBlockMatch(firstFileLines, secondFileLines));
	}
	
	/**
	 * Merges code block matches. Suppose that
	 * 
	 * match1 = firstFile[10, 20] secondFile[40, 62]
	 * match2 = firstFile[15, 22] secondFile[62, 80]
	 * 
	 * Function creates a new match.
	 * 
	 * match3 = firstFile[10, 22] secondFile[40, 80]
	 * 
	 * Deletes match1 and match2 from codeBlockMatches list and adds match3.
	 * Operates until there are no super positions left.
	 */
	public void mergeCodeBlocks()
	{
		CodeBlockMatch cbm1 = null;
		CodeBlockMatch cbm2 = null;
		boolean continueToMerge = false;
		
		if(codeBlockMatches.size() < 2) return;
		
		for(int i=0; i<codeBlockMatches.size()-1; i++)
		{
			if(i+1 < codeBlockMatches.size())
			for(int j=i+1; j<codeBlockMatches.size(); j++)
			{
				cbm1 = codeBlockMatches.get(i);
				cbm2 = codeBlockMatches.get(j);
				if(doesSuperPositionExist(cbm1.getFirstFileLines(), cbm2.getFirstFileLines()) &&
					doesSuperPositionExist(cbm1.getSecondFileLines(), cbm2.getSecondFileLines()))
				{
					continueToMerge = true;
					Line firstFileLines = new Line(
						Math.min(cbm1.getFirstFileLines().getStartLine(), cbm2.getFirstFileLines().getStartLine()), 
						Math.max(cbm1.getFirstFileLines().getEndLine(), cbm2.getFirstFileLines().getEndLine()));
					
					Line secondFileLines = new Line(
						Math.min(cbm1.getSecondFileLines().getStartLine(), cbm2.getSecondFileLines().getStartLine()), 
						Math.max(cbm1.getSecondFileLines().getEndLine(), cbm2.getSecondFileLines().getEndLine()));
					
					CodeBlockMatch newCodeBlockMatch = new CodeBlockMatch(firstFileLines, secondFileLines);
					codeBlockMatches.add(newCodeBlockMatch);
					codeBlockMatches.remove(i);
					codeBlockMatches.remove(j-1);
					
					break;
				}
			}
		}
		if(continueToMerge) mergeCodeBlocks();
	}
	
	/**
	 * Looks if there is a super position between two lines.
	 * @param l1 (Line) : first line
	 * @param l2 (Line) : second line
	 * @returns true if there is a super position, false otherwise.
	 */
	private boolean doesSuperPositionExist(Line l1, Line l2)
	{
		if(((l2.getStartLine() >= l1.getStartLine()) && (l2.getStartLine() <= l1.getEndLine())) || 
			((l1.getStartLine() >= l2.getStartLine()) && (l1.getStartLine() <= l2.getEndLine())))
			return true;
		
		return false;
	}
	
	/**
	 * Calculates the similar line count and sets file1SimilarLineCount, file2SimilarLineCount members.
	 * For performance reasons, code blocks should be merged before using this function.
	 */
	public void calculateSimilarLineCount()
	{
		int[] file1SimilarLines = new int[file1TotalLineCount];
		int[] file2SimilarLines = new int[file2TotalLineCount];
		
		for(CodeBlockMatch cbm : codeBlockMatches)
		{
			fillArr(file1SimilarLines, cbm.getFirstFileLines());
			fillArr(file2SimilarLines, cbm.getSecondFileLines());
		}
		
		file1SimilarLineCount = 0;
		for(int i : file1SimilarLines)
			if(i == 1) ++file1SimilarLineCount;
		
		file2SimilarLineCount = 0;
		for(int i : file2SimilarLines)
			if(i == 1) ++ file2SimilarLineCount;
	}
	
	/**
	 * Fills a parameter with 1 between given line interval.
	 * @param arr (int[])
	 * @param l (Line) : Line interval -> [startLine, endLine].
	 * @returns modified parameter array.
	 */
	private int[] fillArr(int arr[], Line l)
	{
		for(int i=l.getStartLine(); i<l.getEndLine()+1; i++)
			arr[i] = 1;
		
		return arr;
	}
	
	/**
	 * Calculates the similarity ratios for both files.
	 * similarLineCount and fileLineCount members must be set before calling this function.
	 * 
	 * @notice : This function calculates the code segments on line basis. To get more consistent results it should be changed to character basis.  
	 */
	public void calculateSimilarityRatio()
	{
//		float normalizationPortion = (float) Math.sqrt(file1SimilarLineCount * file2SimilarLineCount);
//		float normalizationDenominator = (float) Math.sqrt(file1CodeLineCount * file2CodeLineCount);
//		file1SimilarityRatio = (normalizationPortion / normalizationDenominator) * 100;
//		file2SimilarityRatio = file1SimilarityRatio;
		file1SimilarityRatio = ((float) file1SimilarLineCount / file1TotalLineCount) * 100;
		file2SimilarityRatio = ((float) file2SimilarLineCount / file2TotalLineCount) * 100;
		if(file1SimilarityRatio > 100) file1SimilarityRatio = 100;
		if(file2SimilarityRatio > 100) file2SimilarityRatio = 100;
	}

	// Getter & Setter
	// ------------------------------------------------------------------------------------------------------------------
	
	public String getFile1ID() {
		return file1ID;
	}

	public void setFile1ID(String file1id) {
		file1ID = file1id;
	}

	public String getFile2ID() {
		return file2ID;
	}

	public void setFile2ID(String file2id) {
		file2ID = file2id;
	}
	
	public String getFile1Path() {
		return file1Path;
	}

	public void setFile1Path(String file1Path) {
		this.file1Path = file1Path;
	}

	public String getFile2Path() {
		return file2Path;
	}

	public void setFile2Path(String file2Path) {
		this.file2Path = file2Path;
	}

	public float getFile1SimilarityRatio() {
		return file1SimilarityRatio;
	}

	public void setFile1SimilarityRatio(float file1SimilarityRatio) {
		this.file1SimilarityRatio = file1SimilarityRatio;
	}

	public float getFile2SimilarityRatio() {
		return file2SimilarityRatio;
	}

	public void setFile2SimilarityRatio(float file2SimilarityRatio) {
		this.file2SimilarityRatio = file2SimilarityRatio;
	}

	public int getFile1TotalLineCount1() {
		return file1TotalLineCount;
	}

	public void setFile1TotalLineCount1(int file1TotalLineCount) {
		this.file1TotalLineCount = file1TotalLineCount;
	}

	public int getFile2TotalLineCount() {
		return file2TotalLineCount;
	}

	public void setFile2TotalLineCount(int file2TotalLineCount) {
		this.file2TotalLineCount = file2TotalLineCount;
	}
	
	public int getFile1CodeLineCount() {
		return file1CodeLineCount;
	}

	public void setFile1CodeLineCount(int file1CodeLineCount) {
		this.file1CodeLineCount = file1CodeLineCount;
	}

	public int getFile2CodeLineCount() {
		return file2CodeLineCount;
	}

	public void setFile2CodeLineCount(int file2CodeLineCount) {
		this.file2CodeLineCount = file2CodeLineCount;
	}

	public int getFile1SimilarLineCount() {
		return file1SimilarLineCount;
	}

	public void setFile1SimilarLineCount(int file1SimilarLineCount) {
		this.file1SimilarLineCount = file1SimilarLineCount;
	}

	public int getFile2SimilarLineCount() {
		return file2SimilarLineCount;
	}

	public void setFile2SimilarLineCount(int file2SimilarLineCount) {
		this.file2SimilarLineCount = file2SimilarLineCount;
	}

	public List<CodeBlockMatch> getCodeBlockMatches() {
		return codeBlockMatches;
	}

	public void setCodeBlockMatches(List<CodeBlockMatch> codeBlockMatches) {
		this.codeBlockMatches = codeBlockMatches;
	}

	public float getNormalizedFileSimilarityRatio() {
		return normalizedFileSimilarityRatio;
	}

	public void setNormalizedFileSimilarityRatio(float normalizedFileSimilarityRatio) {
		this.normalizedFileSimilarityRatio = normalizedFileSimilarityRatio;
	}
}
