package com.weaseldiesel.core.datastructure;

import com.weaseldiesel.bean.uidata.Pair;

public class StudentMatchKey 
{
	private String student1;
	private String student2;
	
	public StudentMatchKey(String student1, String student2)
	{
		this.student1 = student1;
		this.student2 = student2;
	}
	
	public boolean equals(StudentMatchKey anotherKey)
	{
		return (this.student1.equals(anotherKey.student1) && this.student2.equals(anotherKey.student2)) ||
				(this.student1.equals(anotherKey.student2) && this.student2.equals(anotherKey.student1));
	}

	public boolean equals(Pair pair)
	{
		return (this.student1.equals(pair.getData1()) && this.student2.equals(pair.getData2())) ||
				(this.student1.equals(pair.getData2()) && this.student2.equals(pair.getData1()));
	}
	
	public String getStudent1() {
		return student1;
	}

	public void setStudent1(String student1) {
		this.student1 = student1;
	}

	public String getStudent2() {
		return student2;
	}

	public void setStudent2(String student2) {
		this.student2 = student2;
	}
}
