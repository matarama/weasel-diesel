package com.weaseldiesel.core.datastructure;

/**
 * Contains a list of file names which include syntax errors.
 */

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ErrorLog extends ObjectSerializer implements Serializable
{
	private static final long serialVersionUID = 20L;
	
	private List<String> fileNames;
	private String path;
	
	//Constructors
	public ErrorLog() 
	{
		super();
		this.fileNames = new ArrayList<String>();
		this.path = null;
	}
	
	public ErrorLog(String path) 
	{
		super();
		this.fileNames = new ArrayList<String>();
		this.path = path;
	}
	
	
	public boolean serialize()
	{
		return this.serializeObject(this, new File(path));
	}
	
	public void addFileNames(String fileName)
	{
		this.fileNames.add(fileName);
	}

	//Getter & Setter
	
	public List<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(List<String> fileNames) {
		this.fileNames = fileNames;
	}
	
}
