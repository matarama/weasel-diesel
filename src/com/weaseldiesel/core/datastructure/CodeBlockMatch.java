package com.weaseldiesel.core.datastructure;

import java.io.Serializable;

public class CodeBlockMatch implements Serializable
{
	private static final long serialVersionUID = 6L;
	
	private Line firstFileLines;
	private Line secondFileLines;
	
	public CodeBlockMatch() 
	{
		super();
		this.firstFileLines = null;
		this.secondFileLines = null;
	}

	public CodeBlockMatch(Line firstFileLines, Line secondFileLines) 
	{
		super();
		this.firstFileLines = firstFileLines;
		this.secondFileLines = secondFileLines;
	}
	
	public Line getFirstFileLines() {
		return firstFileLines;
	}

	public void setFirstFileLines(Line firstFileLines) {
		this.firstFileLines = firstFileLines;
	}

	public Line getSecondFileLines() {
		return secondFileLines;
	}

	public void setSecondFileLines(Line secondFileLines) {
		this.secondFileLines = secondFileLines;
	}
}
