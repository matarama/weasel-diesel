package com.weaseldiesel.core.datastructure;

import java.io.Serializable;

public class Line implements Serializable
{
	private static final long serialVersionUID = 8L;
	
	private int startLine;
	private int endLine;
	
	public Line() 
	{
		super();
		this.startLine = 0;
		this.endLine = 0;
	}
	
	public Line(int startLine, int endLine) 
	{
		super();
		this.startLine = startLine;
		this.endLine = endLine;
	}

	public Line(String lineString)
	{
		super();
		
		String[] lineNumbers = lineString.split(",");
		this.startLine = Integer.parseInt(lineNumbers[0]);
		this.endLine = Integer.parseInt(lineNumbers[1]);
	}
	
	public String toString(){
		return "Line[" + this.startLine + ", " + this.endLine + "]";
	}
	
	public int getStartLine() {
		return startLine;
	}

	public void setStartLine(int startLine) {
		this.startLine = startLine;
	}

	public int getEndLine() {
		return endLine;
	}

	public void setEndLine(int endLine) {
		this.endLine = endLine;
	}	
}
