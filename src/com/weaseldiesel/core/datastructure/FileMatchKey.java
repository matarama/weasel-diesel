package com.weaseldiesel.core.datastructure;

import com.weaseldiesel.bean.uidata.Pair;

public class FileMatchKey 
{
	private String file1;
	private String file2;
	
	public FileMatchKey(String file1, String file2) 
	{
		super();
		this.file1 = file1;
		this.file2 = file2;
	}
	
	public boolean equals(FileMatchKey anotherKey)
	{
		return (this.getFile1().equals(anotherKey.getFile1()) && this.getFile2().equals(anotherKey.getFile2()) ||
				this.getFile2().equals(anotherKey.getFile1()) && this.getFile1().equals(anotherKey.getFile2()));
	}
	
	public boolean equals(Pair pair)
	{
		return (this.getFile1().equals(pair.getData1()) && this.getFile2().equals(pair.getData2()) ||
				this.getFile2().equals(pair.getData1()) && this.getFile1().equals(pair.getData2()));
	}

	public String getFile1() {
		return file1;
	}

	public void setFile1(String file1) {
		this.file1 = file1;
	}

	public String getFile2() {
		return file2;
	}

	public void setFile2(String file2) {
		this.file2 = file2;
	}
}
