package com.weaseldiesel.core.datastructure;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.fileSystem.FileSystem;
import com.weaseldiesel.core.utility.LineExtractor;

public class Fingerprint extends ObjectSerializer implements Serializable
{
	private static final long serialVersionUID = 10L;
	
	private String fileName;
	private List<HashValue> hashValues;
	private List<Line> lineNumbers;
	private int lineCount;
	private int codeLineCount;
	
	public Fingerprint() 
	{
		super();
		this.fileName = null;
		this.hashValues = null;
		this.lineNumbers = null;
		this.lineCount = 0;
		this.codeLineCount = 0;
	}
	
	/**
	 * Creates the fingerprint of given file. Automatically calculates the line numbers for each fingerprint.
	 * i.th element's line numbers in hashValues is stored in the i.th element of line numbers.
	 * @param fileName: String -> file path of the document.
	 * @param hashValues: List<HashValue> -> the result of document fingerprinting.
	 * @param kgram: integer -> code block size (in terms of character).
	 */
	public Fingerprint(String fileName, List<HashValue> hashValues, int kgram,
				int lineCount, int codeLineCount) 
	{
		super();
		this.fileName = fileName;
		this.hashValues = hashValues;
		this.lineNumbers = extractLineNumbers(kgram);
		this.lineCount = lineCount;
		this.codeLineCount = codeLineCount;
	}
		
	/**
	 * Creates the fingerprint of given file.
	 * i.th element's line numbers in hashValues is stored in the i.th element of line numbers.
	 * @param fileName: String -> file path of the document.
	 * @param hashValues: List<Pair> -> the result of document fingerprinting.
	 * @param lineNumbers: List<Line> -> line number for each <hashValue,position> pair.
	 */
	public Fingerprint(String fileName, List<HashValue> hashValues,
			List<Line> lineNumbers) 
	{
		super();
		this.fileName = fileName;
		this.hashValues = hashValues;
		this.lineNumbers = lineNumbers;
		this.lineCount = 0;
		this.codeLineCount = 0;
	}
	
	/**
	 * Applies extractLineNumber function for per hash value. Returns a list containing line numbers for all hash values.
	 * @param kgram: integer -> code block size (in terms of character)
	 * @return List<Line> -> Start-End line numbers per code block
	 */
	public List<Line> extractLineNumbers(int kgram)
	{
		List<Line> lineNumbers = new ArrayList<Line>();
		
		String fileBuffer = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			String line = reader.readLine();
			while(line != null)
			{
				fileBuffer += line;
				line = reader.readLine();
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		for(HashValue p : hashValues)
			lineNumbers.add(new Line(LineExtractor.extractLineNumber(fileBuffer, p.getStartPosition(), p.getEndPosition())));
		
		return lineNumbers;
	}
	
	/**
	 * Writes given fingerprint to student's fingerprint folder using Serializable interface.
	 * @param fp (Fingerprint) : fingerprint to be written.
	 * @param originalCodeFilePath (String) : To determine the location of fingerprint.
	 * @returns true if operation succeeds, false otherwise.
	 */
	public boolean serialize(String fileName)
	{
		return serializeObject(this, new File(fileName));
	}

	
	/**
	 * Reads a fingerprint from previously serialized file and returns it.
	 * @param filePath (String) : File path of the fingerprint.
	 * @return Fingerprint if the operation succeeds, null otherwise.
	 */
	public static Fingerprint readFingerprint(String filePath)
	{
		return (Fingerprint) FileSystem.readSerializedFile(filePath);
	}

	/**
	 * Reads the previously serialized fingerprints from given file paths and generates a FileMatch object by comparing hash values of those fingerprints.
	 * 
	 * @param f1Path (String)
	 * @param f2Path (String)
	 * 
	 * @returns a FileMatch object containing the result of comparison between hash values of both fingerprints. Note that this object may not include any code block matches.
	 */
	public static FileMatch compareFingerprints(String f1Path, String f2Path)
	{
		Fingerprint fp1 = readFingerprint(f1Path);
		Fingerprint fp2 = readFingerprint(f2Path);
		FileMatch fileMatch = new FileMatch(f1Path, f2Path, fp1.getLineCount(), fp2.getLineCount(), 
								fp1.getCodeLineCount(), fp2.getCodeLineCount());
		
		for(int i=0; i<fp1.getHashValues().size(); i++)
		{
			for(int j=0; j<fp2.getHashValues().size(); j++)
			{
				if(fp1.getHashValues().get(i).getHashValue() == fp2.getHashValues().get(j).getHashValue())
					fileMatch.addCodeBlockMatch(fp1.getLineNumbers().get(i), fp2.getLineNumbers().get(j));
			}
		}
		fileMatch.calculateSimilarLineCount();
		fileMatch.calculateSimilarityRatio();
		return fileMatch;
	}
	
	public String toString()
	{
		String s = "";
		for(int i=0; i<hashValues.size(); i++)
		{
			s += hashValues.get(i) + " - " + lineNumbers.get(i) + "\n";
		}
		
		return s;
	}
	
	//Getter & Setter
	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<HashValue> getHashValues() {
		return hashValues;
	}

	public void setHashValues(List<HashValue> hashValues) {
		this.hashValues = hashValues;
	}

	public List<Line> getLineNumbers() {
		return lineNumbers;
	}

	public void setLineNumbers(List<Line> lineNumbers) {
		this.lineNumbers = lineNumbers;
	}

	public int getLineCount() {
		return lineCount;
	}

	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

	public int getCodeLineCount() {
		return codeLineCount;
	}

	public void setCodeLineCount(int codeLineCount) {
		this.codeLineCount = codeLineCount;
	}
}
