package com.weaseldiesel.core.datastructure;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.weaseldiesel.core.main.Parameter;

import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.core.utility.Utility;

import com.weaseldiesel.core.fileSystem.FileSystem;

public class StudentMatch extends ObjectSerializer implements Serializable 
{
	private static final long serialVersionUID = 4L;
	
	private String student1Path;
	private String student2Path;
	private String student1Id;
	private String student2Id;
	private List<FileMatch> fileMatches;
	private float student1SimilarityRatio;
	private float student2SimilarityRatio;
	private float normalizedSimilarityRatio;
	private float signatureSimilarityRatio;
	
	//Constructors
	
	public StudentMatch() 
	{
		super();
		this.student1Path = null;
		this.student2Path = null;
		this.student1Id = null;
		this.student2Id = null;
		this.fileMatches = null;
		this.student1SimilarityRatio = 0;
		this.student2SimilarityRatio = 0;
		this.signatureSimilarityRatio = 0;
	}
	
	public StudentMatch(String student1Path, String student2Path) 
	{
		super();
		this.student1Path = student1Path;
		this.student2Path = student2Path;
		this.student1Id = student1Path.substring(student1Path.lastIndexOf("\\") + 1);
		this.student2Id = student2Path.substring(student2Path.lastIndexOf("\\") + 1);
		this.fileMatches = new ArrayList<FileMatch>();
		this.student1SimilarityRatio = 0;
		this.student2SimilarityRatio = 0;
		this.signatureSimilarityRatio = 0;
	}
	
	public StudentMatch(String student1Path, String student2Path,
			String student1Id, String student2Id) 
	{
		super();
		this.student1Path = student1Path;
		this.student2Path = student2Path;
		this.student1Id = student1Id;
		this.student2Id = student2Id;
		this.fileMatches = null;
		this.student1SimilarityRatio = 0;
		this.student2SimilarityRatio = 0;
		this.signatureSimilarityRatio = 0;
	}
	
	public StudentMatch(String student1Path, String student2Path,
			String student1Id, String student2Id, List<FileMatch> fileMatches,
			float student1SimilarityRatio, float student2SimilarityRatio,
			float signatureSimilarityRatio) 
	{
		super();
		this.student1Path = student1Path;
		this.student2Path = student2Path;
		this.student1Id = student1Id;
		this.student2Id = student2Id;
		this.fileMatches = fileMatches;
		this.student1SimilarityRatio = student1SimilarityRatio;
		this.student2SimilarityRatio = 0;
		this.signatureSimilarityRatio = 0;
	}
	
	/**
	 * Adds a given FileMatch object to list.
	 * 
	 * @param fileMatch (FileMatch)
	 * 
	 * @returns the given FileMatch for callback purposes.
	 */
	public FileMatch addFileMatch(FileMatch fileMatch)
	{
		if(fileMatch.getFile1SimilarityRatio() < Parameter.getTresholdFileSimilarityRatio() &&
			fileMatch.getFile2SimilarityRatio() < Parameter.getTresholdFileSimilarityRatio())
			return null;
		
		if(fileMatch.getCodeBlockMatches().size() == 0)
			return null;
		
		fileMatches.add(fileMatch);
		return fileMatch;
	}
	
	/**
	 * Serialize this object for both students. Object will be serialized in students' matches directory.
	 * 
	 * Removes all the fileMatch objects that have similarity ratio below treshold. If there are no fileMatch objects left then the object will not be serialized.
	 * 
	 * Student similarity ratio is calculated for display purposes.
	 * 
	 * @returns true if the object serialized successfully, false otherwise.
	 */
	public boolean serialize()
	{
		for(int i=0; i<fileMatches.size(); i++)
		{
			if(fileMatches.get(i).getFile1SimilarityRatio() < Parameter.getTresholdFileSimilarityRatio() &&
				fileMatches.get(i).getFile2SimilarityRatio() < Parameter.getTresholdFileSimilarityRatio())
				fileMatches.remove(i);
		}
		
		boolean ret = false;
		if(fileMatches.size() != 0 && 
			student1SimilarityRatio >= Parameter.getTresholdStudentSimilarityRatio() &&
			student2SimilarityRatio >= Parameter.getTresholdStudentSimilarityRatio())
		{
			ret = serializeForStudent1();
			ret = ret && serializeForStudent2();
		}
		
		return ret;
	}
	
	/**
	 * Calculates and sets the member student similarity ratio.
	 * 
	 * st1SimilarityRatio = st1SimilarLineCount / st1TotalLineCount
	 * st2SimilarityRatio = st2SimilarLineCount / st2TotalLineCount
	 * 
	 * studentSimilarityRatio = (st1SimilarityRatio + st2SimilarityRatio) / 2
	 */
	public void calculateStudentSimilarityRatio()
	{
		setStudent1SimilarityRatio(0);
		setStudent2SimilarityRatio(0);
		int student1TotalNumberOfCodeLines = 0;
		int student1TotalNumberOfSimilarCodeLines = 0;
		int student2TotalNumberOfCodeLines = 0;
		int student2TotalNumberOfSimilarCodeLines = 0;
		
		student1TotalNumberOfSimilarCodeLines = getTotalSimilarLineCount(false);
		student2TotalNumberOfSimilarCodeLines = getTotalSimilarLineCount(true);
		
		student1TotalNumberOfCodeLines = getTotalLineCount(student1Path + "\\" + Keyword.CODE_FILES);
		student2TotalNumberOfCodeLines = getTotalLineCount(student2Path + "\\" + Keyword.CODE_FILES);
		
		student1SimilarityRatio = ((float) student1TotalNumberOfSimilarCodeLines / student1TotalNumberOfCodeLines) * 100;
		student2SimilarityRatio = ((float) student2TotalNumberOfSimilarCodeLines / student2TotalNumberOfCodeLines) * 100;
		
		float normalizationDenominator = (float) Math.sqrt(student1TotalNumberOfCodeLines * student2TotalNumberOfCodeLines);
		
		student1SimilarityRatio = ((float) student1TotalNumberOfSimilarCodeLines / normalizationDenominator) * 100;
		normalizedSimilarityRatio = ((float) ((student1TotalNumberOfSimilarCodeLines + student2TotalNumberOfSimilarCodeLines) / 2) / normalizationDenominator) * 100;
		
		if(student1SimilarityRatio > 100) 
			student1SimilarityRatio = 100;
		if(student2SimilarityRatio > 100) 
			student2SimilarityRatio = 100;
	}
	
	public void calculateSignatureSimilarityRatio()
	{
		Signature s1 = Signature.readSignature(student1Path + "\\" + Keyword.SIGNATURE + "\\" + Keyword.SIGNATURE + Keyword.SERIALIZATION_EXTENSION);
		Signature s2 = Signature.readSignature(student2Path + "\\" + Keyword.SIGNATURE + "\\" + Keyword.SIGNATURE + Keyword.SERIALIZATION_EXTENSION);
		
		this.signatureSimilarityRatio = s1.compare(s2);
	}
	
	/**
	 * Serialized this object for first student.
	 * 
	 * @returns true if the object serialized successfully, false otherwise.
	 */
	private boolean serializeForStudent1()
	{
		String fileName = student1Path + "\\" + Keyword.MATCHES + "\\" + student2Id + Keyword.SERIALIZATION_EXTENSION;
		File file = new File(fileName);
		return serializeObject(this, file);
	}
	
	/**
	 * Serialized this object for second student.
	 * 
	 * @returns true if the object serialized successfully, false otherwise.
	 */
	private boolean serializeForStudent2()
	{
		String fileName = student2Path + "\\" + Keyword.MATCHES + "\\" + student1Id + Keyword.SERIALIZATION_EXTENSION;	
		File file = new File(fileName);
		return serializeObject(this, file);
	}
	
	/**
	 * To calculate student scoring system and if a file has more than 1 match, this function finds the match who has the highest match score and ignores all others when calculating student match score.
	 * @param filePath
	 * @return
	 * @notice low perfomance, should be changed.
	 */
	public int getTotalSimilarLineCount(boolean forStudent2)
	{
		int ret = 0;
		Map<String, Byte> map = new HashMap<String, Byte>();
		if(forStudent2)
		{
			for(FileMatch fm : fileMatches)
				map.put(fm.getFile2Path(), (byte) 0);
			
			for(Entry<String, Byte> e : map.entrySet())
				ret += getBiggestSimilarLineCountForSt2File(e.getKey());
		}
		else
		{
			for(FileMatch fm : fileMatches)
				map.put(fm.getFile1Path(), (byte) 0);
			
			for(Entry<String, Byte> e : map.entrySet())
				ret += getBiggestSimilarLineCountForSt1File(e.getKey());
		}
		return ret;
	}
	
	/**
	 * To calculate student scoring system and if a file has more than 1 match, this function finds the match who has the highest match score and ignores all others when calculating student match score.
	 * @param filePath
	 * @return
	 * @notice low perfomance, should be changed.
	 */
	private int getBiggestSimilarLineCountForSt1File(String filePath)
	{
		int ret = 0;
		float ratio = 0;
		for(FileMatch fm : fileMatches)
			if(fm.getFile1Path().equals(filePath))
				if(ratio < fm.getFile1SimilarityRatio())
				{
					ratio = fm.getFile1SimilarityRatio();
					ret = fm.getFile1SimilarLineCount();
				}
		
		return ret;
	}
	
	/**
	 * To calculate student scoring system and if a file has more than 1 match, this function finds the match who has the highest match score and ignores all others when calculating student match score.
	 * @param filePath
	 * @return
	 * @notice low perfomance, should be changed.
	 */
	private int getBiggestSimilarLineCountForSt2File(String filePath)
	{
		int ret = 0;
		float ratio = 0;
		for(FileMatch fm : fileMatches)
			if(fm.getFile2Path().equals(filePath))
				if(ratio < fm.getFile2SimilarityRatio())
				{
					ratio = fm.getFile2SimilarityRatio();
					ret = fm.getFile2SimilarLineCount();
				}
		
		return ret;
	}
	
	/**
	 * Burda toplam kod sat�r�n� getirmekle ilgili bir mant�k hatas� var.
	 * Bazen bu say� eslenmis sat�r say�lar�ndan y�ksek ��k�yor. Bunun icin sat�r numaralara daha dikkatli eklenmelidir.(parser da)
	 * @param studentPath
	 * @return
	 * @notice
	 */
	public static int getTotalLineCount(String studentPath)
	{
		int ret = 0;
		
		for(String st1Fingerprint : Utility.findSerializedFiles(studentPath))
		{
			Fingerprint fp = (Fingerprint) FileSystem.readSerializedFile(st1Fingerprint);
//			ret += fp.getCodeLineCount();
			ret += fp.getLineCount();
		}
		
		
		return ret;
	}
	
	//Getter & Setter
	
	public String getStudent1Path() {
		return student1Path;
	}

	public void setStudent1Path(String student1Path) {
		this.student1Path = student1Path;
	}

	public String getStudent2Path() {
		return student2Path;
	}

	public void setStudent2Path(String student2Path) {
		this.student2Path = student2Path;
	}

	public String getStudent1Id() {
		return student1Id;
	}

	public void setStudent1Id(String student1Id) {
		this.student1Id = student1Id;
	}

	public String getStudent2Id() {
		return student2Id;
	}

	public void setStudent2Id(String student2Id) {
		this.student2Id = student2Id;
	}

	public List<FileMatch> getFileMatches() {
		return fileMatches;
	}

	public void setFileMatches(List<FileMatch> fileMatches) {
		this.fileMatches = fileMatches;
	}

	public float getStudent1SimilarityRatio() {
		return student1SimilarityRatio;
	}

	public void setStudent1SimilarityRatio(float student1SimilarityRatio) {
		this.student1SimilarityRatio = student1SimilarityRatio;
	}

	public float getStudent2SimilarityRatio() {
		return student2SimilarityRatio;
	}

	public void setStudent2SimilarityRatio(float student2SimilarityRatio) {
		this.student2SimilarityRatio = student2SimilarityRatio;
	}

	public float getSignatureSimilarityRatio() {
		return signatureSimilarityRatio;
	}

	public void setSignatureSimilarityRatio(float signatureSimilarityRatio) {
		this.signatureSimilarityRatio = signatureSimilarityRatio;
	}

	public float getNormalizedSimilarityRatio() {
		return normalizedSimilarityRatio;
	}

	public void setNormalizedSimilarityRatio(float normalizedSimilarityRatio) {
		this.normalizedSimilarityRatio = normalizedSimilarityRatio;
	}
}
