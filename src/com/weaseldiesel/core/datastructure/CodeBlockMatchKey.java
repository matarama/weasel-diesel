package com.weaseldiesel.core.datastructure;

import com.weaseldiesel.bean.uidata.Pair;

public class CodeBlockMatchKey 
{
	private String cbm1;
	private String cbm2;
	
	public CodeBlockMatchKey(String cbm1, String cbm2) 
	{
		super();
		this.cbm1 = cbm1;
		this.cbm2 = cbm2;
	}
	
	public boolean equals(CodeBlockMatchKey anotherKey)
	{
		return (this.getCbm1().equals(anotherKey.getCbm1()) && this.getCbm2().equals(anotherKey.getCbm2())) ||
				(this.getCbm2().equals(anotherKey.getCbm1()) && this.getCbm1().equals(anotherKey.getCbm2()));
	}
	
	public boolean equals(Pair pair)
	{
		return (this.getCbm1().equals(pair.getData1()) && this.getCbm2().equals(pair.getData2())) ||
				(this.getCbm2().equals(pair.getData1()) && this.getCbm1().equals(pair.getData2()));
	}

	// Getter & Setter
	// -------------------------------------------------------------
	
	public String getCbm1() {
		return cbm1;
	}

	public void setCbm1(String cbm1) {
		this.cbm1 = cbm1;
	}

	public String getCbm2() {
		return cbm2;
	}

	public void setCbm2(String cbm2) {
		this.cbm2 = cbm2;
	}
}
