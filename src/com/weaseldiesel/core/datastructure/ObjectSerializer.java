package com.weaseldiesel.core.datastructure;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import com.weaseldiesel.core.fileSystem.FileSystem;

public abstract class ObjectSerializer 
{
	/**
	 * Writes given object to give file using Serializable interface.
	 * @param obj (Object) : Object to be written.
	 * @param file (File)
	 * @returns true if operation succeeds, false otherwise.
	 */
	protected boolean serializeObject(Object obj, File file)
	{
		String directoryPath = FileSystem.getParentDirectory(file.getAbsolutePath());
		File directory = new File(directoryPath);
		if(!directory.exists())
			directory.mkdirs();
		
		FileOutputStream fos = null;
		ObjectOutputStream ous = null;
		try
		{
			fos = new FileOutputStream(file);
			ous = new ObjectOutputStream(fos);
			ous.writeObject(obj);
			fos.close();
			ous.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
}
