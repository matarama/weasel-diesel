package com.weaseldiesel.core.datastructure;

import java.io.File;
import java.io.Serializable;
import java.util.List;

import com.weaseldiesel.core.fileSystem.FileSystem;

public class Signature extends ObjectSerializer implements Serializable
{
	private static final long serialVersionUID = 7L;
	
	private String studentSignaturePath;
	private List<String> variableNames;
	private List<String> comments;
	private List<String> methodNames;
	private List<String> stringLiterals;
	
	//Constructors
	
	public Signature() 
	{
		super();
		this.studentSignaturePath = null;
		this.variableNames = null;
		this.comments = null;
		this.methodNames = null;
		this.stringLiterals = null;
	}
	
	public Signature(String studentFolderName) 
	{
		super();
		this.studentSignaturePath = studentFolderName;
		this.variableNames = null;
		this.comments = null;
		this.methodNames = null;
		this.stringLiterals = null;
	}
	
	public Signature(String studentSignaturePath, List<String> variableNames,
			List<String> comments, List<String> methodNames,
			List<String> stringLiterals) {
		super();
		this.studentSignaturePath = studentSignaturePath;
		this.variableNames = variableNames;
		this.comments = comments;
		this.methodNames = methodNames;
		this.stringLiterals = stringLiterals;
	}

	/**
	 * Compares this object to another object of the same class.
	 * 
	 * @param other (Signature).
	 * 
	 * @returns a value representing signature similarity ratio (between 0 and 100).
	 */
	public float compare(Signature other)
	{
		float result = 0;
		
		int similarMethodNameCount = 0;
		float methodNameNormalizationDenominator = (float) 
					Math.sqrt(this.methodNames.size() * other.getMethodNames().size());
		for(String s1 : methodNames)
			for(String s2 : other.getMethodNames())
				if(s1.equals(s2))
				{
					++similarMethodNameCount;
					continue;
				}
		//float methodSimilarityRatio = (float) similarMethodNameCount / methodNameNormalizationDenominator;

		
		int similarVariableNameCount = 0;
		float variableNameNormalizationDenominator = (float) 
					Math.sqrt(this.variableNames.size() * other.getVariableNames().size());
		for(String s1 : variableNames)
			for(String s2 : other.getVariableNames())
				if(s1.equals(s2))
				{
					++similarVariableNameCount;
					continue;
				}
		//float variableSimilarityRatio = (float) similarVariableNameCount / variableNameNormalizationDenominator;
		
		
		int similarLiteralCount = 0;
		float literalNormalizationDenominator = (float) 
					Math.sqrt(this.stringLiterals.size() * other.getStringLiterals().size());
		for(String s1 : this.stringLiterals)
			for(String s2 : other.getStringLiterals())
				if(s1.equals(s2))
				{
					++similarLiteralCount;
					continue;
				}
		//float literalSimilarityRatio = (float) similarLiteralCount / literalNormalizationDenominator;
		
		
		int similarCommentCount = 0;
		float commentNormalizationDenominator = (float) 
					Math.sqrt(this.comments.size() * other.getComments().size());
		for(String s1 : comments)
			for(String s2 : other.getComments())
				if(s1.equals(s2))
				{
					++similarCommentCount;
					continue;
				}
		//float commentSimilarityRatio = (float) similarCommentCount / commentNormalizationDenominator;
		
		
		if(similarCommentCount == 0) similarCommentCount = 1;
		if(similarLiteralCount == 0) similarLiteralCount = 1;
		if(similarMethodNameCount == 0) similarMethodNameCount = 1;
		if(similarVariableNameCount == 0) similarVariableNameCount = 1;
		
		//result = ((variableSimilarityRatio + commentSimilarityRatio + literalSimilarityRatio + methodSimilarityRatio) / 4) * 100;
		
		result = 100 * (similarCommentCount * similarLiteralCount * similarMethodNameCount * similarVariableNameCount) /
			(commentNormalizationDenominator * literalNormalizationDenominator * 
			methodNameNormalizationDenominator * variableNameNormalizationDenominator);
		
		if(result > 100 || result < 0)
			return 100;
		
		return result;
	}

	/**
	 * Writes the contents of this object to a file using java's Serializable interface.
	 * File path is the fileName attribute of the class.
	 * @return
	 */
	public boolean serialize()
	{
		File file = new File(this.studentSignaturePath);
		return serializeObject(this, file);
	}
	
	/**
	 * Reads a signature from previously serialized file and returns it.
	 * @param filePath (String) : File path of the fingerprint.
	 * @return Signature if the operation succeeds, null otherwise.
	 */
	public static Signature readSignature(String filePath)
	{
		return (Signature) FileSystem.readSerializedFile(filePath);
	}
	
	//Getter & Setter


	public String getStudentSignaturePath() {
		return studentSignaturePath;
	}

	public void setStudentSignaturePath(String studentSignaturePath) {
		this.studentSignaturePath = studentSignaturePath;
	}

	public List<String> getVariableNames() {
		return variableNames;
	}

	public void setVariableNames(List<String> variableNames) {
		this.variableNames = variableNames;
	}

	public List<String> getComments() {
		return comments;
	}

	public void setComments(List<String> comments) {
		this.comments = comments;
	}

	public List<String> getMethodNames() {
		return methodNames;
	}

	public void setMethodNames(List<String> methodNames) {
		this.methodNames = methodNames;
	}

	public List<String> getStringLiterals() {
		return stringLiterals;
	}

	public void setStringLiterals(List<String> stringLiterals) {
		this.stringLiterals = stringLiterals;
	}
}
