package com.weaseldiesel.core.algorithm;

import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.core.datastructure.HashValue;

public class RabinKarpRollingHash 
{

	public static final int PRIME_BASE = 251; //251 - 101
	public static final int PRIME_MOD = 1000000007;
	private static int startPosition = 0;
	private static int endPosition = 0;
	
	
	/**
	 * Utility function.
	 * Checks if the parameter character is intentionally embedded by the parser.
	 * Those characters will not be included when calculating hash values. 
	 * @param c
	 * @return true if the character should be ignored, false otherwise.
	 */
	private static boolean checkIfIgnored(char c)
	{
		if(c == Keyword.IGNORE || Character.isDigit(c))
			return true;
		return false;
	}
	
	/**
	 * Regular hash function. Calculates hash value for the given string.
	 * This method is used if the k_gram >= s.length.
	 * @param s
	 * @return (Long) hash value.
	 */
	public static long hash(String s)
	{
		long value = 0;
		
		for(int i=0; i<s.length(); i++)
		{
			while(checkIfIgnored(s.charAt(i)))
			{
				++i;
				if(i >= s.length())
					break;
			}
			if(i >= s.length())
				return value;
				
			value = value * PRIME_BASE + (int) s.charAt(i);
			value %= PRIME_MOD;
		}
		
		return value;
	}
	
	/**
	 * This method will be called when file length > k_gram.
	 * Method calculates the hash value for first l chars (ignored chars are not included).
	 * @param s -> (String)fileBuffer
	 * @param l -> (int) k_gram
	 * @return
	 */
	public static long solidHash(String s, int l)
	{
		long value = 0;
		int length = l;
		for(int i=0; i<s.length(); i++)
			if(!checkIfIgnored(s.charAt(i)))
			{
				startPosition = i;
				break;
			}
		
		for(int i=startPosition; i<length+startPosition; i++)
		{
			while(checkIfIgnored(s.charAt(i)))
			{
				++i;
				++length;
				if(i >= s.length())
					return value;
			}
			
			value = value * PRIME_BASE + (int) s.charAt(i);
			value %= PRIME_MOD;
		}
		
		endPosition = length + startPosition - 1;
		return value;
	}
	
	
	/**
	 * Divides the parameter string into k_grams.
	 * So that the second character of the first gram is the first character of the first gram.
	 * Time Complexity: O(max(k_gram,s.length())) = O(n)
	 * @param s -> FileBuffer
	 * @param k_gram -> size of the sub parts
	 * @return List<HashValue> containing hash values with start, end positions for all document sub parts.
	 */
	public static List<HashValue> getHashValues(String s, int k_gram)
	{
		List<HashValue> hashValues = new ArrayList<HashValue>();
		if(s.length() <= k_gram)
		{
			hashValues.add(new HashValue(hash(s), 0, s.length()-1));	
		}
		else
		{
			long hash = solidHash(s, k_gram);
			hashValues.add(new HashValue(hash, startPosition, endPosition));
			
			long power = 1;
			for(int i=0; i<k_gram; i++)
				power = (power * PRIME_BASE) % PRIME_MOD;
			
			int i = startPosition + 1;
			while(i < s.length())
			{
				if(i >= s.length())
					break;
				if(!checkIfIgnored(s.charAt(i)))
				{
					//get the letter to be added
					int lastLetterIndex = endPosition + 1;
					if(lastLetterIndex >= s.length())
						break;
					while(checkIfIgnored(s.charAt(lastLetterIndex)))
					{
						++lastLetterIndex;
						if(lastLetterIndex >= s.length())
							break;
					}
					
					if(lastLetterIndex < s.length())
					{
						//add letter
						hash = hash * PRIME_BASE + (int)s.charAt(lastLetterIndex);
						hash %= PRIME_MOD;
						endPosition = lastLetterIndex;
						
						//delete first letter
						hash -= power * ((int)s.charAt(startPosition)) % PRIME_MOD;
						if(hash < 0) 
							hash += PRIME_MOD;
						startPosition = i;
						
						hashValues.add(new HashValue(hash, startPosition, endPosition));
					}
					else
						break;			
				}
				++i;
			}
		}
		
		return hashValues;
	}
}
