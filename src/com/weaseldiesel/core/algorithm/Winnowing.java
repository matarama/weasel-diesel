package com.weaseldiesel.core.algorithm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import com.weaseldiesel.core.datastructure.HashValue;
import com.weaseldiesel.core.datastructure.Window;

public class Winnowing 
{
	public static final int INT_MAX = RabinKarpRollingHash.PRIME_MOD -1;
	
	/**
	 * Applies winnowing algorithm to given hash values. Returns a list of hash values containing document fingerprint.
	 * @param t : at least t-gram matches must be detected.
	 * @param k : any matches smaller than k-gram will be ignored.
	 * @param hashes : hash values of a given document.
	 * @return fingerprint of the given hash values.
	 */
	public static List<HashValue> winnow(int t, int k, List<HashValue> hashes)
	{	
		Window window = new Window(t, k);
		List<HashValue> fingerprint = new ArrayList<HashValue>();
		
		double p = window.getD(); //density
		
		List<HashValue> smallestHashValues = window.getMinimumValuesPerWindow(hashes);

		int numberOfWindows = smallestHashValues.size();
		int numberOfFingerprints = (int) Math.ceil(p * numberOfWindows);
		
		//To avoid infinite loop: if the nubmer of hash values we select is bigger then 
		//the different numbers we have then store only the different numbers
		HashMap<Integer, HashValue> map = new HashMap<Integer, HashValue>();
		for(HashValue x : smallestHashValues)
			map.put(x.getStartPosition(), x);
		
		if(map.size() > numberOfFingerprints)
		{
			while(numberOfFingerprints > 0)
			{
				HashValue hashValue = null;
				do
				{
					hashValue = smallestHashValues.get(chooseRandomIndex(smallestHashValues.size()));
					smallestHashValues.remove(hashValue);
				} while(contains(fingerprint, hashValue));
				
				fingerprint.add(hashValue);
				--numberOfFingerprints;
			}
		}
		else {
			for(Entry<Integer, HashValue> e : map.entrySet())
				fingerprint.add(e.getValue());
		}
		
		return fingerprint;
	}
	
	/**
	 * Utility function.
	 * This method is used to create a random number to select a fingerprint.
	 * @param upperLimit
	 * @return
	 */
	private static int chooseRandomIndex(int upperLimit)
	{
		return (int) ((Math.random() * 1000000) % upperLimit);
	}
	
	/**
	 * Utility function.
	 * Used to determine if a given hash value exists in a give list.
	 * @param l
	 * @param pair
	 * @return
	 */
	private static boolean contains(List<HashValue> l, HashValue hashValue)
	{
		for(HashValue p : l)
			if(p.getStartPosition() == hashValue.getStartPosition())
				return true;
		
		return false;
	}
}
