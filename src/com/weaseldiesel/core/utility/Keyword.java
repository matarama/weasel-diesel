package com.weaseldiesel.core.utility;

/**
 * A class which includes all the keywords which are used for creating file system. 
 *
 */

public class Keyword 
{
	public static final String CODE_FILES = "Code Files";
	public static final String SIGNATURE = "Signature";
	public static final String FINGERPRINTS = "Fingerprints";
	public static final String MODIFIED_CODE_FILES = "Modified Code Files";
	public static final String ORIGINAL_CODE_FILES = "Original Code Files";
	public static final String MATCHES = "Matches";
	public static final String SOURCE_FOLDER = "src";
	public static final String REFLECTION = "reflection";
	public static final String ERROR_LOG = "error_log";
	
	public static final String SERIALIZATION_EXTENSION = ".ser";
	public static final String JAVA_EXTENSION = ".java";
	public static final String ZIP_EXTENSION = ".zip";
	public static final String TEXT_EXTENSION = ".txt";
	
	public static final char IGNORE = ';'; 
}
