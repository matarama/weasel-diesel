package com.weaseldiesel.core.utility;

public class LineExtractor
{
	public static final char IGNORE = ';';
	
	/**
	 * Robust & Efficient function that extracts start-end line numbers within given interval (= startPos - endPos).
	 * If interval does not contain any line numbers function finds the line number by using file buffer.
	 * @param fileBuffer : File buffered in a string.
	 * @param startPos : Start position of the string interval.
	 * @param endPos : End position of the string interval.
	 * @return (String) Line Number -> "startLine,endLine"
	 */
	public static String extractLineNumber(String fileBuffer, int startPos, int endPos)
	{	
		if(indexOfRegex(fileBuffer) < 0) 
			return null;		
		
		String numbers = null;
		String realStr = null;
		if(fileBuffer.length() < endPos)
			realStr = fileBuffer;
		else
			realStr = fileBuffer.substring(startPos, endPos);
		
		//get start line
		numbers = getStartLine(fileBuffer, startPos, endPos, realStr);
		
		//gen end line
		int start = lastIndexOfRegex(realStr) + 1;
		if(start <= 0)
			numbers += "," + numbers;
		else{
			int end = realStr.substring(start).indexOf(IGNORE) + start;
			numbers += "," + realStr.substring(start, end);
		}
		
		return numbers;
	}
	
	/**
	 * Utility Function that extracts starting line for given code block.
	 * @param s : File buffered in a string.
	 * @param startPos : start position of an interval.
	 * @param endPos : end position of an interval.
	 * @param realStr : fileBuffer.substring(startPos, endPos).
	 * @return (String) startLine -> "startLine".
	 */
	public static String getStartLine(String s, int startPos, int endPos, String realStr)
	{
		int firstRegexIndex = indexOfRegex(realStr);
		if(firstRegexIndex == -1 || firstRegexIndex != 0)
		{
			int firstSemicolon = realStr.indexOf(IGNORE);
			if(firstSemicolon < 0)
				firstSemicolon = startPos;
			else
				firstSemicolon += startPos;
			
			String temp = s.substring(0, firstSemicolon + 1);
			firstRegexIndex = lastIndexOfRegex(temp);

			if(firstRegexIndex == -1) 
				return "-1";
			
			int secondSemicolon = temp.substring(firstRegexIndex + 1).indexOf(IGNORE) + firstRegexIndex + 1;
			return temp.substring(firstRegexIndex+1, secondSemicolon);
		}
		else
		{
			int secondSemicolon = realStr.substring(firstRegexIndex+1).indexOf(IGNORE) + firstRegexIndex + 1;
			return realStr.substring(firstRegexIndex+1, secondSemicolon);
		}
	}
	
	/**
	 * A Utility function which checks if a given string includes the regular expression (;(\\d)+;).
	 * If given string includes the regular expression then returns the starting index of the first occurrence. -1 otherwise.
	 * @param source
	 * @return (integer) start index of the regular expression (;(\\d)+;).
	 */
	public static int indexOfRegex(String source)
	{
		boolean found = false;
		int start = -1;
		for(int i=0; i<source.length(); i++)
		{
			if(source.charAt(i) == IGNORE)
			{
				start = i++;
				if(i >= source.length()) return -1;
				boolean digitFound = false;
				
				while(Character.isDigit(source.charAt(i)))
				{
					++i;
					digitFound = true;
					if(i >= source.length()) return -1;
				}
				if(i >= source.length()) return -1;
				if(source.charAt(i) == IGNORE)
				{
					if(digitFound)
					{
						found = true;
						break;
					}
					else
						--i;
				}
			}
		}
		
		if(found)
			return start;
		return -1;
	}
	
	/**
	 * A Utility function which checks if a given string includes the regular expression (;(\\d)+;).
	 * If given string includes the regular expression then returns the starting index of the last occurrence. -1 otherwise.
	 * @param source
	 * @return (integer) start index of the regular expression (;(\\d)+;).
	 */
	public static int lastIndexOfRegex(String source)
	{
		boolean found = false;
		int start = -1;
		for(int i=source.length()-1; i>=0; i--)
		{
			if(source.charAt(i) == IGNORE)
			{
				--i;
				if(i < 0) return -1;
				boolean digitFound = false;
				
				while(Character.isDigit(source.charAt(i)))
				{
					--i;
					digitFound = true;
					if(i < 0) return -1;
				}
				if(source.charAt(i) == IGNORE)
				{
					if(digitFound)
					{
						found = true;
						start = i;
						break;
					}
					else 
						++i;
				}
			}
		}
		
		if(found)
			return start;
		return -1;
	}	
}
