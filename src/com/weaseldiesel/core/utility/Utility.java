package com.weaseldiesel.core.utility;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.weaseldiesel.core.main.Parameter;

public class Utility 
{
	/**
	 * Finds serialized object files in given folder path. Returns absolute path of all serialized object files contained in this folder as a list.
	 * @param sourceFolderPath (String) : Path of the folder to be searched.
	 * @return (List<String>) : Paths of java code files.
	 */
	public static List<String> findSerializedFiles(String sourceFolderPath)
	{
		List<String> serializedFilePaths = new ArrayList<String>();
		File source = new File(sourceFolderPath);
		for(String fileName : source.list())
		{
			File file = new File(sourceFolderPath + "\\" + fileName);
			if(file.isDirectory())
			{
				serializedFilePaths.addAll(findSerializedFiles(file.getAbsolutePath()));
			}
			else if(isSerializedFile(file))
			{
				serializedFilePaths.add(file.getAbsolutePath());
			}
			
		}
		
		return serializedFilePaths;
	}
	
	/**
	 * Checks if given file is a serialized object.
	 * @param f (File)
	 * @return
	 */
	public static boolean isSerializedFile(File f)
	{
		int length = f.getAbsolutePath().length();
		String extension = f.getAbsolutePath().substring(length - 4);
		
		return (extension.equals(Keyword.SERIALIZATION_EXTENSION) && f.isFile());
	}
	
	/**
	 * Finds java code files in given folder path. Returns absolute path of all java code files contained in this folder as a list.
	 * @param sourceFolderPath (String) : Path of the folder to be searched.
	 * @return (List<String>) : Paths of java code files.
	 */
	public static List<String> findJavaCodeFiles(String sourceFolderPath)
	{
		List<String> javaFilePaths = new ArrayList<String>();
		File source = new File(sourceFolderPath);
		for(String fileName : source.list())
		{
			File file = new File(sourceFolderPath + "\\" + fileName);
			if(file.isDirectory())
			{
				javaFilePaths.addAll(findJavaCodeFiles(file.getAbsolutePath()));
			}
			else if(isJavaCodeFile(file))
			{
				javaFilePaths.add(file.getAbsolutePath());
			}
			
		}
		
		return javaFilePaths;
	}
	
	/**
	 * Checks if given file is a java code file.
	 * @param f (File)
	 * @return
	 */
	public static boolean isJavaCodeFile(File f)
	{
		int length = f.getName().length();
		String extension = f.getName().substring(length - 5);
		
		return (extension.equals(Keyword.JAVA_EXTENSION) && f.isFile());
	}
	
	/**
	 * Parses file name from given path without extension of that file.
	 * @param path
	 * @return
	 */
	public static String getFileNameFromPath(String path)
	{
		
		String fileName = path.indexOf("\\") < 0 ? path : path.substring(path.lastIndexOf("\\") + 1);
		if(fileName.indexOf('.') >= 0)
			fileName = fileName.substring(0, fileName.lastIndexOf('.'));
		
		return fileName; 
	}
	
	public static String getZipFilePath(String path)
	{
		String courseName = getFileNameFromPath(path);
		return Parameter.getDownloadFolderPath() + courseName + Keyword.ZIP_EXTENSION;
	}
	
	public static <K, V> List<V> convertFromMapToList(Map<K, V> map)
	{
		List<V> list = new ArrayList<V>();
		
		for(Entry<K, V> e : map.entrySet())
			list.add(e.getValue());
		
		return list;
	}
	
	public static String getJavaFromSer(String ser)
	{
		return ser.substring(0, ser.lastIndexOf('.')) + Keyword.JAVA_EXTENSION;
	}
	
	public static String findJavaPath(String path)
	{
		path = path.replace(Keyword.FINGERPRINTS, Keyword.ORIGINAL_CODE_FILES);
		path = path.substring(0, path.lastIndexOf('.')) + Keyword.JAVA_EXTENSION;
		return path;
	}
}
