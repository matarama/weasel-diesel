package com.weaseldiesel.core.main;

import java.util.List;

import com.weaseldiesel.core.algorithm.RabinKarpRollingHash;
import com.weaseldiesel.core.algorithm.Winnowing;
import com.weaseldiesel.core.datastructure.ErrorLog;
import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.datastructure.Fingerprint;
import com.weaseldiesel.core.datastructure.HashValue;
import com.weaseldiesel.core.datastructure.StudentMatch;
import com.weaseldiesel.core.fileSystem.FileSystem;

import com.weaseldiesel.core.parser.interpreter.JavaParser;
import com.weaseldiesel.core.parser.interpreter.ParseException;
import com.weaseldiesel.core.parser.utility.FileBuffer;
import com.weaseldiesel.core.parser.utility.SignatureBuffer;

import com.weaseldiesel.core.reflection.CodeFile;
import com.weaseldiesel.core.reflection.Course;
import com.weaseldiesel.core.reflection.Student;
import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.core.utility.Utility;

public class ProgramControl extends Thread 
{

	private static float studentTotalSimilarity = 0;
	
	public static void main(String[] args) 
	{
  		//FileSystem fileSystem = createFileSystem("test21234");
		COExecute("test_data");
	}
	
	public static void COExecute(String coursePath)
    {
        FileSystem fileSystem = ProgramControl.createFileSystem(coursePath);
        Course reflection = ProgramControl.createReflection(fileSystem);
        ProgramControl.generateFingerprintsAndSignatures(reflection);
        ProgramControl.compareStudents(reflection);
        ProgramControl.addStudentMatchesToReflection(reflection);
        ProgramControl.calculateCourseCopyRatio(reflection);
        reflection.serialize();
    }

	public static void calculateCourseCopyRatio(Course reflection)
	{
		reflection.setCoursecopyRatio(studentTotalSimilarity / (float)(Math.pow(reflection.getStudents().size(), 2) / 2));
	}
	
	public static FileSystem createFileSystem(String courseZipPath)
	{
		FileSystem f = new FileSystem(courseZipPath.substring(0, courseZipPath.length()));
		f.unzipMainFolder();
		f.unzipCourseFolder();
		f.deleteTemporaryFiles();
		
		return f;
	}
	
	public static Course createReflection(FileSystem f)
	{
		Course course = new Course(f.getCourseFilePath(), f.getStudentDirectories());
		for(Student st : course.getStudents())
		{
			List<String> codeFiles = Utility.findJavaCodeFiles(st.getStudentPath());
			st.addCodeFile(codeFiles);
		}
		
		return course;
	}
	
	public static void generateFingerprintsAndSignatures(Course reflection)
	{
		for(Student st : reflection.getStudents())
		{
			SignatureBuffer.flushBuffer();
			ErrorLog errorLog = new ErrorLog(st.getErrorLogPath());
			for(CodeFile cf : st.getCodeFiles())
			{
				try {
					new JavaParser(cf.getOriginalCodeFilePath());
				} catch (ParseException e) {
					String fileName = cf.getOriginalCodeFilePath();
					fileName = fileName.substring(fileName.lastIndexOf
							(Keyword.ORIGINAL_CODE_FILES) + Keyword.ORIGINAL_CODE_FILES.length() + 1);
					errorLog.addFileNames(fileName);
					//e.printStackTrace();
				} catch (Exception e) {
					//e.printStackTrace();
				}
				FileBuffer.writeBufferToFile(cf.getModifiedCodeFilePath());
				cf.setLineCount(FileBuffer.getLineCount());
				List<HashValue> hashValues = Winnowing.winnow(Parameter.getT(), Parameter.getK(), 
						RabinKarpRollingHash.getHashValues(FileBuffer.getFileBuffer(), Parameter.getK()));
				Fingerprint fp = new Fingerprint(cf.getModifiedCodeFilePath(), hashValues, Parameter.getK(), 
						FileBuffer.getLineCount(), FileBuffer.getTotalCodeLineCount());
				fp.serialize(cf.getFingerprintPath());
			}
			SignatureBuffer.serializeSignature(st.getSignaturePath());
			errorLog.serialize();
		}
	}
	
	public static void compareStudents(Course reflection)
	{
		List<Student> students = reflection.getStudents();
		for(int i=0; i<students.size()-1; i++)
		{
			for(int j=i+1; j<students.size(); j++)
			{
				List<CodeFile> firstStudentCodeFiles = students.get(i).getCodeFiles();
				List<CodeFile> secondStudentCodeFiles = students.get(j).getCodeFiles();
				compareCodeFiles(students.get(i).getStudentPath(), firstStudentCodeFiles, 
						students.get(j).getStudentPath(), secondStudentCodeFiles);
			}
		}
	}
	
	private static void compareCodeFiles(String firstStudentPath, List<CodeFile> firstStudentCodeFiles, 
									String secondStudentPath, List<CodeFile> secondStudentCodeFiles)
	{
		StudentMatch studentMatch = new StudentMatch(firstStudentPath, secondStudentPath);
		for(int i=0; i<firstStudentCodeFiles.size(); i++)
		{
			for(int j=0; j<secondStudentCodeFiles.size(); j++)
			{
				FileMatch fm = Fingerprint.compareFingerprints(
						firstStudentCodeFiles.get(i).getFingerprintPath(), 
						secondStudentCodeFiles.get(j).getFingerprintPath());
				fm.mergeCodeBlocks();
				studentMatch.addFileMatch(fm);
				
			}
		}
		studentMatch.calculateStudentSimilarityRatio();
		studentMatch.calculateSignatureSimilarityRatio();
		float normalizationDenominator = (float) 
			Math.sqrt(studentMatch.getStudent1SimilarityRatio() * studentMatch.getStudent2SimilarityRatio());
		studentTotalSimilarity += studentMatch.getStudent1SimilarityRatio() / normalizationDenominator;
		studentTotalSimilarity += studentMatch.getStudent2SimilarityRatio() / normalizationDenominator;
		studentMatch.serialize();
	}
	
	public static void addStudentMatchesToReflection(Course reflection)
	{
		for(Student st : reflection.getStudents())
			st.setMatches(Utility.findSerializedFiles(st.getMatchesPath()));
	}
	
	
}
