package com.weaseldiesel.core.main;

/**
 * Class for customization. Simply set the values.
 */

public class Parameter 
{
	private static int t = 50;
	private static int k = 40;

	private static float tresholdFileSimilarityRatio = 40;
	private static float tresholdStudentSimilarityRatio = 20;

	private static String mainFolderPath = "C:\\Users\\guanu\\workspace\\WeaselDiesel\\issues\\";
	private static String downloadFolderPath = "C:\\Users\\guanu\\workspace\\WeaselDiesel\\downloads\\";
	
	//Getter & Setter
	
	public static float getTresholdFileSimilarityRatio() {
		return tresholdFileSimilarityRatio;
	}
	
	public static void setTresholdFileSimilarityRatio(
			float tresholdFileSimilarityRatio) {
		Parameter.tresholdFileSimilarityRatio = tresholdFileSimilarityRatio;
	}
	
	public static float getTresholdStudentSimilarityRatio() {
		return tresholdStudentSimilarityRatio;
	}
	
	public static void setTresholdStudentSimilarityRatio(
			float tresholdStudentSimilarityRatio) {
		Parameter.tresholdStudentSimilarityRatio = tresholdStudentSimilarityRatio;
	}
	
	public static int getT() {
		return t;
	}
	
	public static int getK() {
		return k;
	}

    public static void setT(int t) {
		Parameter.t = t;
	}

	public static void setK(int k) {
		Parameter.k = k;
	}

	public static String getMainFolderPath() {
		return mainFolderPath;
	}

	public static void setMainFolderPath(String mainFolderPath) {
		Parameter.mainFolderPath = mainFolderPath;
	}

	public static String getDownloadFolderPath() {
		return downloadFolderPath;
	}

	public static void setDownloadFolderPath(String downloadFolderPath) {
		Parameter.downloadFolderPath = downloadFolderPath;
	}
}
