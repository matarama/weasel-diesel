package com.weaseldiesel.core.main;

import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.weaseldiesel.core.datastructure.CodeBlockMatch;
import com.weaseldiesel.core.datastructure.ErrorLog;
import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.datastructure.StudentMatch;
import com.weaseldiesel.core.datastructure.StudentMatchKey;
import com.weaseldiesel.core.fileSystem.FileSystem;
import com.weaseldiesel.core.reflection.Course;
import com.weaseldiesel.core.reflection.Student;
import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.core.utility.Utility;

public class ProgramDisplay implements Serializable
{
	private static final long serialVersionUID = 4660334756687032058L;
	
	private Course reflection;
	private Map<StudentMatchKey, StudentMatch> studentMatchBufferMap;
	private Map<String, Integer> studentMatchCountMap;
	private List<FileMatch> currentFileMatches;
	private List<CodeBlockMatch> currentCodeBlockMatches;
	private float currentStudent1MatchRatio;
	private float currentStudent2MatchRatio;
	private float currentStudentsSignatureSimilarityRaito;
	
	public ProgramDisplay() 
	{
		super();
		this.reflection = null;
		this.studentMatchCountMap = new HashMap<String, Integer>();
		this.studentMatchBufferMap = new HashMap<StudentMatchKey, StudentMatch>();
		this.currentFileMatches = null;
		this.currentCodeBlockMatches = null;
		this.currentStudent1MatchRatio = 0;
		this.currentStudent2MatchRatio = 0;
		this.currentStudentsSignatureSimilarityRaito = 0;
	}
	
	public Student findStudent(String studentId)
	{
		for(Student s : reflection.getStudents())
			if(s.getStudentID().equals(studentId))
				return s;
				
		return null;
	}

	public String getMatchName(Student s ,int index)
	{
		return Utility.getFileNameFromPath(s.getMatches().get(index));
	}
	
	/**
	 * 
	 * @param courseName
	 * @returns true is program finds the course, false otherwise.
	 */
	public boolean readCourseReflection(String courseName)
	{
		
		String reflectionPath = Parameter.getMainFolderPath() + "\\" + courseName + "\\" + Keyword.REFLECTION + Keyword.SERIALIZATION_EXTENSION;
		if(!new File(reflectionPath).exists())
			return false;
		
		reflection = (Course) FileSystem.readSerializedFile(reflectionPath);
		fillStudentMatchCountMap();
		
		return true;
	}	
	
	private void fillStudentMatchCountMap()
	{
		for(Student student : reflection.getStudents())
			if(student.hasMatches())
				studentMatchCountMap.put(student.getStudentID(), student.getMatchCount());
	}
	
	public ErrorLog readStudentErrorLog(String path)
	{
		return (ErrorLog) FileSystem.readSerializedFile(path);
	}
	
	public StudentMatch readStudentMatch(StudentMatchKey key)
	{
		//If already buffered
		if(studentMatchBufferMap.containsKey(key))
			return studentMatchBufferMap.get(key);
		
		//else read student match and set current items and add to buffer.
		for(Student st : reflection.getStudents())
		{
			if(st.getStudentPath().substring(st.getStudentPath().lastIndexOf('\\') + 1).equals(key.getStudent1()))
			{
				for(String s2 : st.getMatches())
				{
					if(s2.substring(s2.lastIndexOf('\\') + 1, s2.length() - 4).equals(key.getStudent2()))
					{
						StudentMatch sm = (StudentMatch) FileSystem.readSerializedFile(s2);
						
						studentMatchBufferMap.put(key, sm);
						currentFileMatches = sm.getFileMatches();
						currentStudent1MatchRatio = sm.getStudent1SimilarityRatio();
						currentStudent2MatchRatio = sm.getStudent2SimilarityRatio();
						currentStudentsSignatureSimilarityRaito = sm.getSignatureSimilarityRatio();
						return sm;
					}
				}
			}
		}
		
		return null;
	}
	
	public StudentMatch readStudentMatch(String student1Id, String student2Id)
	{
		return readStudentMatch(new StudentMatchKey(student1Id, student2Id));
	}
	
	public void getCodeBlockMatches(String filePath1, String filePath2)
	{
		for(FileMatch fm : currentFileMatches)
			if(fm.getFile1Path().equals(filePath1) || fm.getFile2Path().equals(filePath2))
				if(fm.getFile1Path().equals(filePath2) || fm.getFile2Path().equals(filePath1))
				{
					currentCodeBlockMatches = fm.getCodeBlockMatches();
					break;
				}
	}

	public static void main(String[] args)
	{
		ProgramDisplay pd = new ProgramDisplay();
		pd.readCourseReflection("test2");
			
		for(int i=0; i<pd.getReflection().getStudents().size(); i++)
		{
			String s1 = pd.getReflection().getStudents().get(i).getStudentPath();
			String st1 = s1.substring(s1.lastIndexOf('\\') + 1);
			
			System.out.println();
			System.out.println("Student " + s1 + " matches:");
			
			for(String s2 : pd.getReflection().getStudents().get(i).getMatches())
			{
				String st2 = s2.substring(s2.lastIndexOf('\\') + 1, s2.length()-4);
				
				pd.readStudentMatch(st1, st2);
				
				System.out.println("Structure Match Ratio: " + pd.getCurrentStudent1MatchRatio());
				System.out.println("Signature Match Ratio: " + pd.getCurrentStudentsSignatureSimilarityRaito());
				
				for(FileMatch fm : pd.getCurrentFileMatches())
				{
					System.out.println(fm.getFile1Path().substring(s1.lastIndexOf('\\') + 1) + ": " + 
							fm.getFile1SimilarityRatio() + " - " + 
							fm.getFile2Path().substring(s1.lastIndexOf('\\') + 1) + ": " + 
							fm.getFile2SimilarityRatio());
				}
				
				ErrorLog el = pd.readStudentErrorLog(pd.getReflection().getStudents().get(i).getErrorLogPath());
				for(String s : el.getFileNames())
					System.out.println("Error: Document " + s + " cannot be parsed. Parser Version: JDK 1.5");
			}
		}
	}
	
	//Getter & Setter
	public Course getReflection() {
		return reflection;
	}

	public void setReflection(Course reflection) {
		this.reflection = reflection;
	}
	
	public List<FileMatch> getCurrentFileMatches() {
		return currentFileMatches;
	}

	public void setCurrentFileMatches(List<FileMatch> currentFileMatches) {
		this.currentFileMatches = currentFileMatches;
	}

	public List<CodeBlockMatch> getCurrentCodeBlockMatches() {
		return currentCodeBlockMatches;
	}

	public void setCurrentCodeBlockMatches(
			List<CodeBlockMatch> currentCodeBlockMatches) {
		this.currentCodeBlockMatches = currentCodeBlockMatches;
	}

	public float getCurrentStudent1MatchRatio() {
		return currentStudent1MatchRatio;
	}

	public void setCurrentStudent1MatchRatio(float currentStudent1MatchRatio) {
		this.currentStudent1MatchRatio = currentStudent1MatchRatio;
	}

	public float getCurrentStudent2MatchRatio() {
		return currentStudent2MatchRatio;
	}

	public void setCurrentStudent2MatchRatio(float currentStudent2MatchRatio) {
		this.currentStudent2MatchRatio = currentStudent2MatchRatio;
	}

	public float getCurrentStudentsSignatureSimilarityRaito() {
		return currentStudentsSignatureSimilarityRaito;
	}

	public void setCurrentStudentsSignatureSimilarityRaito(
			float currentStudentsSignatureSimilarityRaito) {
		this.currentStudentsSignatureSimilarityRaito = currentStudentsSignatureSimilarityRaito;
	}

	public Map<StudentMatchKey, StudentMatch> getStudentMatchBufferMap() {
		return studentMatchBufferMap;
	}

	public void setStudentMatchBufferMap(
			Map<StudentMatchKey, StudentMatch> studentMatchBufferMap) {
		this.studentMatchBufferMap = studentMatchBufferMap;
	}

	public Map<String, Integer> getStudentMatchCountMap() {
		return studentMatchCountMap;
	}

	public void setStudentMatchCountMap(Map<String, Integer> studentMatchCountMap) {
		this.studentMatchCountMap = studentMatchCountMap;
	}
}
