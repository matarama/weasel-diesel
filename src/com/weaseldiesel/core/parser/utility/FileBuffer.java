package com.weaseldiesel.core.parser.utility;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import com.weaseldiesel.core.utility.Keyword;

public class FileBuffer 
{	
	private static String fileBuffer;
	private static int lineNumber;
	private static boolean efficient;
	private static int lastAppendedCount;
	private static String lastAppendedToken;
	private static int previousLength;
	private static boolean recording = false;
	private static int totalCodeLineCount;
	
	public static int getLineCount()
	{
		return lineNumber;
	}
	
	public static int getTotalCodeLineCount()
	{
		return totalCodeLineCount;
	}
	
	public static void startRecording()
	{
		recording = true;
		totalCodeLineCount = 0;
		lastAppendedCount = 0;
		previousLength = 0;
	}
	
	public static void stopRecording()
	{
		recording = false;
	}
	
	public static void appendToBuffer(String s)
	{
		fileBuffer += s.toLowerCase();
		lastAppendedToken = s;
		efficient = true;
		if(recording)
		{
			lastAppendedCount += s.length();
			efficient = false;
		}
	}
	
	public static void appendToBufferNormal(String s)
	{
		fileBuffer += s + "";
		lastAppendedToken = s;
		efficient = true;
		if(recording)
		{
			lastAppendedCount += s.length();
			efficient = false;
		}
	}
	
	public static void removeLastAppended()
	{
		fileBuffer = fileBuffer.substring(0, fileBuffer.length() - lastAppendedCount);
	}
	
	public static void writeBufferToFile(String filePath)
	{	
		String folderPath = eliminateLastFile(filePath);
		File folder = new File(folderPath);
		if(!folder.exists())
			folder.mkdirs();
		
		FileOutputStream fos = null;
		PrintWriter writer = null;
		try {
			fos = new FileOutputStream(filePath);
			writer = new PrintWriter(fos);
			writer.print(fileBuffer);
			
			writer.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static String eliminateLastFile(String path)
	{
		int index = path.lastIndexOf('\\');
		return path.substring(0, index);
	}
	
	public static void recordNewFile()
	{
		fileBuffer = Keyword.IGNORE + "1" + Keyword.IGNORE;
		lineNumber = 2;
		
	}
	
	public static void increaseLineNumber()
	{
		++lineNumber;
		if(efficient)
		{
			increaseTotalCodeLineCount();
			fileBuffer += (Keyword.IGNORE + "" + lineNumber + "" + Keyword.IGNORE);
			efficient = false;
			lastAppendedCount += (Keyword.IGNORE + "" + lineNumber + "" + Keyword.IGNORE).length();
		}
	}
	
	public static void increaseLineNumberWithoutRecording()
	{
		++lineNumber;
	}
	
	public static void exchangeLastAppendedTextTokenWise(String exchange)
	{
		int index = fileBuffer.lastIndexOf(lastAppendedToken);
		if(index > -1)
		{
			fileBuffer = fileBuffer.substring(0, index) + 
							exchange + fileBuffer.substring(index + lastAppendedToken.length());
		}
	}

	public static void increaseTotalCodeLineCount()
	{
		++totalCodeLineCount;
	}
	
	public static void eliminateDots()
	{
		int index = fileBuffer.indexOf("."); 
		while(index >= 0)
		{
			fileBuffer = fileBuffer.substring(0, index) + fileBuffer.substring(index+1);
			index = fileBuffer.indexOf("."); 
		}
	}
/*	
	public static void printBuffer()
	{
		System.out.println(fileBuffer);
	}
*/		
	public static void saveCurrentStart()
	{
		previousLength = fileBuffer.length();
	}
	
	public static void replacePreviousStartWith(char c)
	{
		fileBuffer = fileBuffer.substring(0, previousLength) + c + fileBuffer.substring(previousLength + 1);
	}
	
	public static String getFileBuffer() {
		return fileBuffer;
	}

	public static void setFileBuffer(String fileBuffer) {
		FileBuffer.fileBuffer = fileBuffer;
	}

	public static String getLastAppendedToken() {
		return lastAppendedToken;
	}

	public static void setLastAppendedToken(String lastAppendedToken) {
		FileBuffer.lastAppendedToken = lastAppendedToken;
	}
}
