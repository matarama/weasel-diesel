package com.weaseldiesel.core.parser.utility;

import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.datastructure.Signature;



public class SignatureBuffer 
{
	private static String regularBuffer;
	private static List<String> variableNames = new ArrayList<String>();
	private static List<String> methodNames = new ArrayList<String>();
	private static List<String> comments = new ArrayList<String>();
	private static List<String> stringLiterals = new ArrayList<String>();
	
	public static void flushBuffer()
	{
		variableNames = new ArrayList<String>();
		methodNames = new ArrayList<String>();
		comments = new ArrayList<String>();
		stringLiterals = new ArrayList<String>();
	}
	
	public static void setRegularBuffer(String s)
	{
		regularBuffer = s;
	}
	
	public static String getRegularBuffer()
	{
		return regularBuffer;
	}
	
	public static void addRegularBufferTo(List<String> l)
	{
		l.add(regularBuffer);
	}
	
	public static void addToVariableNames(String s) {
		if(s == null) return;
		variableNames.add(s.toLowerCase());
	}
	
	public static void addToMethodNames(String s) {
		if(s == null) return;
		methodNames.add(s.toLowerCase());
	}
	
	public static void addToStringLiterals(String s) {
		if(s == null) return;		
		stringLiterals.add(s.toLowerCase());
	}
	
	public static void addToComments(String s) {
		if(s == null) return;
		comments.add(s.toLowerCase());
	}
	
	public static void serializeSignature(String studentPath)
	{
		new Signature(studentPath, variableNames, methodNames, comments, stringLiterals).serialize();
		
		flushBuffer();
	}
/*	
	public static void printBuffer()
	{
		System.out.println("Variable Names:");
		System.out.println(variableNames);
		System.out.println("Method Names:");
		System.out.println(methodNames);
		System.out.println("Comments:");
		System.out.println(comments);
		System.out.println("String:");
		System.out.println(stringLiterals);
	}
*/
}
