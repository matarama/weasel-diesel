package com.weaseldiesel.core.fileSystem;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import com.weaseldiesel.core.main.Parameter;

import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.core.utility.Utility;

public class FileSystem 
{
	public static final int BUFFER = 2048;
		
	private String courseName;
	private String courseZipFilePath;
	private String mainPath;
	private String tempFilesPath;
	private String courseFilePath;
	private List<String> studentDirectories;
	
	private File mainFile;
	private File courseFile;
	private File tempFile;
	
	//Constructors
	
	public FileSystem()
	{
		super();
		this.mainPath = Parameter.getMainFolderPath();
		this.courseFilePath = mainPath + "\\course";
		this.tempFilesPath = courseFilePath + "\\temp";
		this.studentDirectories = new ArrayList<String>();
		createMainFileSystem();
	}
	
	public FileSystem(String courseName) 
	{
		super();
		this.courseName = courseName;
		this.courseZipFilePath = Utility.getZipFilePath(courseName);
		this.mainPath = Parameter.getMainFolderPath();
		this.courseFilePath = mainPath + "\\" + courseName;
		this.tempFilesPath = courseFilePath + "\\temp";
		this.studentDirectories = new ArrayList<String>();
		createMainFileSystem();
	}
	
	/**
	 * Creates folders for;
	 * - Main File (in which program will run in) if not exists.
	 * - Course File (if already exists chooses another file name and updates course name.
	 * - Temporary File (will be deleted when file system is created successfully). 
	 */
	private void createMainFileSystem()
	{
		//Main file
		mainFile = new File(mainPath);
		if(!mainFile.exists())
			mainFile.mkdir();
		
		//Course file
		int index=1;
		courseFile = new File(courseFilePath);
		while(courseFile.exists())
		{
			courseFile = new File(courseFilePath + "(" + index + ")");
			++index;
		}
		courseFilePath = courseFile.getAbsolutePath();
		courseFile.mkdir();
		
		//Temp file
		tempFilesPath = courseFilePath + "\\temp";
		tempFile = new File(tempFilesPath);
		tempFile.mkdir();
	}

	/**
	 * Unzips the file (.zip only) specified by the zip file Path into the destination file path.
	 * @param zipFilePath : (String) zip file path.
	 * @param destinationFilePath : (String) destination file path.
	 * @return (String) if the unzipped assignment has src folder in it, returns path to this file, null otherwise.
	 */
	private String unzip(String zipFilePath, String destinationFilePath)
	{
		String srcPath = null;
		try {
			BufferedOutputStream dest = null;
			BufferedInputStream is = null;
			ZipEntry entry;
			ZipFile zipfile = new ZipFile(zipFilePath);
			Enumeration<? extends ZipEntry> e = zipfile.entries();
			while(e.hasMoreElements()) {
				entry = (ZipEntry) e.nextElement();
				is = new BufferedInputStream
				(zipfile.getInputStream(entry));
				int count;
				byte data[] = new byte[BUFFER];
				
				if(entry.isDirectory())
				{
					File temp = new File(destinationFilePath + "\\" + entry.getName());
					temp.mkdirs();
					if(temp.getName().equals(Keyword.SOURCE_FOLDER))
						srcPath = temp.getAbsolutePath();
				}
				else
				{
					FileOutputStream fos = new FileOutputStream(destinationFilePath + "\\" + entry.getName());
					dest = new BufferedOutputStream(fos, BUFFER);
					while ((count = is.read(data, 0, BUFFER)) != -1) {
						dest.write(data, 0, count);
					}
					dest.flush();
					dest.close();
					is.close();
				}
			}
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		return srcPath;
	}

	/**
	 * Extracts the file uploaded by use to a temporary folder (which will be deleted once file system is created successfully).
	 */
	public void unzipMainFolder()
	{
		unzip(courseZipFilePath, getTempFile().getAbsolutePath());
	}
	
	/**
	 * Extracts the course folder and copies java code files to the program's main directory under the course name specified.
	 */
	public void unzipCourseFolder()
	{
		String[] zipFileNames = tempFile.list();
		for(String zipFileName : zipFileNames)
		{
			String studentNumber = zipFileName.substring(0, zipFileName.length() - 4);
			createStudentFolder(studentNumber);
			String unzippedStudentPath = tempFilesPath + "\\" + studentNumber;
			String srcPath = unzip(tempFilesPath + "\\" + zipFileName, unzippedStudentPath);
			String destinationFolder = courseFilePath + "\\" + studentNumber + "\\" + Keyword.CODE_FILES + "\\" + Keyword.ORIGINAL_CODE_FILES;
			studentDirectories.add(studentNumber);

			if(srcPath == null)
			{
				for(String path : Utility.findJavaCodeFiles(unzippedStudentPath))
				{
					String dest = destinationFolder + "\\" + getLastNameFromPath(path);
					copyFile(path, dest);
				}
			}
			else
			{
				copyJavaCodeFilesFromIDESourceFolder(srcPath, destinationFolder);
			}
		}
	}
	
	/**
	 * Copies whole directory while leaving the files and sub-directories unchanged.  
	 * @param sourceDir (String) : Source directory.
	 * @param destinationDir (String) : Destination directory.
	 */
	private void copyJavaCodeFilesFromIDESourceFolder(String src, String dest)
	{
		copyFiles(src, dest);
	}
	
	/**
	 * Copies a single non-directory file.
	 * @param sourceFile (String) : Path of the source file. 
	 * @param destinationFile (String) : Path of the destination file.
	 */
	private void copyFile(String sourceFile, String destinationFile)
	{
		InputStream in = null;
		OutputStream out = null;
		try 
		{
			in = new FileInputStream(sourceFile);
			out = new FileOutputStream(new File(destinationFile));
			
			byte[] buf = new byte[BUFFER];
			int len;
			while ((len = in.read(buf)) > 0){
				out.write(buf, 0, len);
			}
			
			in.close();
			out.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Copies whole directory while leaving the files and sub-directories unchanged.  
	 * @param sourceDir (String) : Source directory.
	 * @param destinationDir (String) : Destination directory.
	 */
	private void copyFiles(String sourceDir, String destinationDir)
	{
		File source = new File(sourceDir);
		for(String sourceFileName : source.list())
		{
			File sourceFile = new File(sourceDir + "\\" + sourceFileName);
			
			if(sourceFile.isDirectory())
			{
				File destinationFile = new File(destinationDir + "\\" + sourceFile.getName());
				destinationFile.mkdirs();
				copyFiles(sourceFile.getAbsolutePath(), destinationFile.getAbsolutePath());
			}
			else
			{
				copyFile(sourceFile.getAbsolutePath(), destinationDir + "\\" + sourceFile.getName());
			}
		}
	}
	
	/**
	 * Creates main directories for a student under course file.
	 * Main Directories:
	 * - student file
	 * 		- code file
	 * 	 		- original code file
	 *   		- modified code file
	 *   		- fingerprints file
	 *  	- signature file
	 *  	- matches file
	 * @param studentNumber : (String) student file name.
	 */
	private void createStudentFolder(String studentNumber)
	{
		File studentFile = new File(courseFilePath + "\\" + studentNumber); studentFile.mkdir();
		File codeFile = new File(studentFile.getAbsolutePath() + "\\" + Keyword.CODE_FILES); codeFile.mkdir();
		File originalCodeFile = new File(codeFile.getAbsolutePath() + "\\" + Keyword.ORIGINAL_CODE_FILES); originalCodeFile.mkdir();
		File modifiedCodeFile = new File(codeFile.getAbsolutePath() + "\\" + Keyword.MODIFIED_CODE_FILES); modifiedCodeFile.mkdir();
		File fingerprintsFile = new File(codeFile.getAbsolutePath() + "\\" + Keyword.FINGERPRINTS); fingerprintsFile.mkdir();
		File signatureFile = new File(studentFile.getAbsolutePath() + "\\" + Keyword.SIGNATURE); signatureFile.mkdir();
		File matchesFile = new File(studentFile.getAbsolutePath() + "\\" + Keyword.MATCHES); matchesFile.mkdir();
	}
	
	/**
	 * Deletes previously created temporary files when extracting user input.
	 */
	public void deleteTemporaryFiles()
	{
		deleteDirectory(tempFilesPath);
		if(tempFile.exists())
		{
			for(String fileName : tempFile.list())
				deleteDirectory(tempFile.getAbsolutePath() + "\\" + fileName);
		}
	}
	
	/**
	 * Returns the last file (or directory) contained in given path.
	 * @param path (String)
	 * @return (String)
	 */
	private static String getLastNameFromPath(String path)
	{
		return path.substring(path.lastIndexOf('\\')+1);
	}
	
	/**
	 * Method for deleting every file in a specified directory path including directory itself.
	 * @param folderPath (String)
	 */
	private static void deleteDirectory(String folderPath)
	{
		File file = new File(folderPath);
		if(file.isDirectory())
		{
			for(String fileName : file.list())
				deleteDirectory(file.getAbsolutePath() + "\\" + fileName);
		}
		file.delete();
	}
	
	/**
	 * Reads an object from previously serialized file and returns it.
	 * @param filePath (String) : File path of the fingerprint.
	 * @return Object if the operation succeeds, null otherwise.
	 */
	public static Object readSerializedFile(String filePath)
	{
		Object o = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		
		try 
		{
			fis = new FileInputStream(new File(filePath));
			ois = new ObjectInputStream(fis);
			o = ois.readObject();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return o;
	}
	
	/**
	 * returns the first parent directory of a given file
	 * @param filePath (String) : File's path
	 * @returns path of the first parent directory of a given file.
	 */
	public static String getParentDirectory(String filePath)
	{
		int index = filePath.lastIndexOf("\\");
		return filePath.substring(0, index);
	}
	
	/**
	 * reads a file and returns its content as a string
	 * @param filePath
	 * @return
	 */
	public static String readFile(String filePath)
	{
		String buffer = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filePath));
			String line = reader.readLine();
			
			while(line != null)
			{
				buffer += line + "\n";
				line = reader.readLine();
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return buffer;
	}
	
	/**
	 * ............ must be removed
	 * @param fingerprint
	 * @return
	 */
	public static String getOriginalFileFromFingerprint(String fingerprint)
	{
		if(fingerprint.indexOf(Keyword.FINGERPRINTS) < 0)
			return fingerprint;
		
		String originalFile = fingerprint.substring(0, fingerprint.indexOf(Keyword.FINGERPRINTS)) +
								Keyword.ORIGINAL_CODE_FILES +
								fingerprint.substring(fingerprint.indexOf(Keyword.FINGERPRINTS) + Keyword.FINGERPRINTS.length(), fingerprint.length() - 4) +
								Keyword.JAVA_EXTENSION;
		
		return originalFile;
	}
	
	//Getter & Setter
	
	public String getCourseName() 
	{
		return courseName;
	}

	public void setCourseName(String courseName) 
	{
		this.courseName = courseName;
	}

	public String getMainPath() 
	{
		return mainPath;
	}

	public void setMainPath(String mainPath) 
	{
		this.mainPath = mainPath;
	}

	public String getTempFilesPath() 
	{
		return tempFilesPath;
	}

	public void setTempFilesPath(String tempFilesPath) 
	{
		this.tempFilesPath = tempFilesPath;
	}

	public String getCourseFilePath() 
	{
		return courseFilePath;
	}

	public void setCourseFilePath(String courseFilePath) 
	{
		this.courseFilePath = courseFilePath;
	}

	public File getMainFile() 
	{
		return mainFile;
	}

	public void setMainFile(File mainFile) 
	{
		this.mainFile = mainFile;
	}

	public File getCourseFile() 
	{
		return courseFile;
	}

	public void setCourseFile(File courseFile) 
	{
		this.courseFile = courseFile;
	}

	public File getTempFile() 
	{
		return tempFile;
	}

	public void setTempFile(File tempFile) 
	{
		this.tempFile = tempFile;
	}

	public List<String> getStudentDirectories() 
	{
		return studentDirectories;
	}

	public void setStudentDirectories(List<String> studentDirectories) 
	{
		this.studentDirectories = studentDirectories;
	}	
	
}
