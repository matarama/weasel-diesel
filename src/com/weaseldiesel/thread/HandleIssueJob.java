package com.weaseldiesel.thread;

import java.util.LinkedList;
import java.util.Queue;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.weaseldiesel.core.fileSystem.FileSystem;
import com.weaseldiesel.core.main.ProgramControl;
import com.weaseldiesel.core.reflection.Course;
import com.weaseldiesel.persistence.Issue;

public class HandleIssueJob implements Job
{
	public static final String THREAD_BUSY = "busy";
	public static final String THREAD_IDLE = "idle";
	
	private static String state;
	private static Queue<Issue> issueQueue;
	
	// Constructors
	// ------------------------------------------------------------------------------
	
	public HandleIssueJob()
	{
		if(issueQueue == null)
		{
			issueQueue = new LinkedList<Issue>();
			state = THREAD_IDLE;
		}
	}
	
	// Methods
	// ------------------------------------------------------------------------------
	
	public void execute(JobExecutionContext context) throws JobExecutionException 
	{	
		if(issueQueue.isEmpty())
			return;
		
		if(!acquireFlag())
			return;
		
		Queue<Issue> queue = issueQueue;
 
		issueQueue = new LinkedList<Issue>();
		
		while(!queue.isEmpty())
		{
			Issue issue = queue.remove();
			String fileName = issue.getId() + "";
			
			FileSystem fileSystem = ProgramControl.createFileSystem(fileName);
	        Course reflection = ProgramControl.createReflection(fileSystem);
	        ProgramControl.generateFingerprintsAndSignatures(reflection);
	        ProgramControl.compareStudents(reflection);
	        ProgramControl.addStudentMatchesToReflection(reflection);
	        ProgramControl.calculateCourseCopyRatio(reflection);
	        reflection.serialize();
		}
		
		giveFlagBack();
	}
	
	private synchronized boolean acquireFlag()
	{
		if(state == THREAD_BUSY)
			return false;
		
		state = THREAD_BUSY;
		return true;
	}
	
	private synchronized boolean giveFlagBack()
	{
		state = THREAD_IDLE;
		return true;
	}
	
	public synchronized void addIssue(Issue issue)
	{
		issueQueue.add(issue);
	}
	
	// Getter & Setter
	// ------------------------------------------------------------------------------
	
	public static Queue<Issue> getIssueQueue() {
		return issueQueue;
	}

	public static void setIssueQueue(Queue<Issue> issueQueue) {
		HandleIssueJob.issueQueue = issueQueue;
	}

	public static String getState() {
		return state;
	}

	public static void setState(String state) {
		HandleIssueJob.state = state;
	}
}
