package com.weaseldiesel.thread;

import java.util.Date;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleTrigger;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.SimpleTriggerImpl;

import com.weaseldiesel.common.Settings;

public class WDScheduler 
{
	private static WDScheduler instance;
	private SchedulerFactory sf;
	private Scheduler scheduler;
	
	// Constructors
	// ----------------------------------------------------------------------------------
	
	protected WDScheduler() throws SchedulerException
	{
		this.sf = new StdSchedulerFactory();
		this.scheduler = sf.getScheduler();
		this.scheduler.start();
	}
	
	public void scheduleHandleIssueJob(int minute)
	{
		JobDetailImpl jobDetail = new JobDetailImpl();
		jobDetail.setName("HandleIssueJob");
		jobDetail.setGroup("HandleIssueJobGroup");
		jobDetail.setJobClass(HandleIssueJob.class);
		
		SimpleTriggerImpl trigger = new SimpleTriggerImpl();
		trigger.setName("handleIssueJobTrigger");
		trigger.setStartTime(new Date(System.currentTimeMillis() + Settings.getProcessIssueMilis()));
		trigger.setRepeatCount(SimpleTrigger.REPEAT_INDEFINITELY);
		trigger.setRepeatInterval(Settings.getProcessIssueMilis());
		
		try 
		{
			scheduler.scheduleJob(jobDetail, trigger);
		} 
		catch (SchedulerException e) 
		{
			e.printStackTrace();
		}
	}
	
	// Getter & Setter
	// ----------------------------------------------------------------------------------
	
	public static WDScheduler getInstance() 
	{
		if(instance == null)
		{
			try 
			{
				instance = new WDScheduler();
			} 
			catch (SchedulerException e) 
			{
				e.printStackTrace();
			}
		}
			
		return instance;
	}
	
	public SchedulerFactory getSf() {
		return sf;
	}

	public void setSf(SchedulerFactory sf) {
		this.sf = sf;
	}

	public Scheduler getScheduler() {
		return scheduler;
	}

	public void setScheduler(Scheduler scheduler) {
		this.scheduler = scheduler;
	}
}
