package com.weaseldiesel.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class Utils 
{
	public static Properties readPropertiesFile(String path)
	{
		Properties properties = new Properties();
		
		FileInputStream fis = null;
		try 
		{
			fis = new FileInputStream(new File(path));
			properties.load(fis);
			fis.close();
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		return properties;
	}
	
	public static String readFile(String path)
    {
        String ret = "";
        BufferedReader reader = null;
        try 
        {
            reader = new BufferedReader(new FileReader(path));
            String line = reader.readLine();
            while(line != null)
            {
                ret += line + "\n";
                line = reader.readLine();
            }
        } 
        catch (FileNotFoundException e) 
        {
        	e.printStackTrace();
        } 
        catch (IOException e) 
        {
            e.printStackTrace();
        }
        
        return ret;
    }
}
