package com.weaseldiesel.common;

import java.io.IOException;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class FacesUtils 
{
	public static final String USER_ID = "user_id";
	public static final String ISSUE_ID = "issue_id";
	public static final String USER = "user";
	public static final String ISSUE = "issue";
	
	public static void addMessage(FacesMessage.Severity severity, String summary, String detail)
	{
		FacesMessage fm = new FacesMessage(severity, summary, detail);
		FacesContext.getCurrentInstance().addMessage(null, fm);
	}
	
	public static void refuseUser()
	{
		try 
		{
			FacesContext.getCurrentInstance().getExternalContext().redirect("login.xhtml");
		} 
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	// Session
	// ----------------------------------------------------------------------------------------
	
	public static Object getFromSession(String key) 
	{
		return FacesUtils.getSessionMap().get(key);
	}
	
	public static void putToSession(String key, Object value)
	{
		FacesUtils.getSessionMap().put(key, value);
	}
	
	public static Map<String, Object> getSessionMap()
	{
		return FacesContext.getCurrentInstance().getExternalContext().getSessionMap();
	}
	
	// Application Context
	// ----------------------------------------------------------------------------------------
	
	public static Object getFromApplicationContext(String key)
	{
		return FacesUtils.getApplicationMap().get(key);
	}
	
	public static void putToApplicationContext(String key, String value)
	{
		FacesUtils.getApplicationMap().put(key, value);
	}
	
	public static Map<String, Object> getApplicationMap()
	{
		return FacesContext.getCurrentInstance().getExternalContext().getApplicationMap();
	}
}
