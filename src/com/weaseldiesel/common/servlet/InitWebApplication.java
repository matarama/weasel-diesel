package com.weaseldiesel.common.servlet;

import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;

import com.weaseldiesel.common.Settings;
import com.weaseldiesel.common.Utils;
import com.weaseldiesel.thread.WDScheduler;

public class InitWebApplication extends HttpServlet 
{
	private static final long serialVersionUID = -2052545396408478688L;

	public void init(ServletConfig serveletConfig) throws ServletException
	{
		super.init(serveletConfig);
		initializeApplication(true);
	}
	
	public void initializeApplication(boolean initializeThreads)
	{
		Properties appProps = 
				Utils.readPropertiesFile(getServletContext().getInitParameter("application-properties"));
		Properties messageProps = 
				Utils.readPropertiesFile(getServletContext().getInitParameter("message-properties"));
		
		Settings.getInstance().fillData(appProps, messageProps);
		
		if(initializeThreads)
		{
			WDScheduler.getInstance().scheduleHandleIssueJob(Settings.getProcessIssueMilis());
		}
	}
}
