package com.weaseldiesel.common;

import java.util.Properties;

import com.weaseldiesel.core.main.Parameter;

public class Settings
{
	public static final String USER_MANUAL_PATH = "resources/docs/user_manual.pdf";
	public static final String COURSE_NAME = "course_name";
	
	private static Properties appProps;
	private static String mainPath;
	private static String downloadPath;
	private static int processIssueMilis;
	
	private static Properties messages;
	
	protected Settings() { /* DO NOTHING */ }
	
	public synchronized void fillData(Properties appProps, Properties messageProps)
	{
		setAppProps(appProps);
		setMessages(messageProps);
		setMainPath(appProps.getProperty("main-path"));
		setDownloadPath(appProps.getProperty("download-path"));
		setProcessIssueMilis(Integer.parseInt(appProps.getProperty("process-issue-milis")));
		
		Parameter.setDownloadFolderPath(downloadPath);
		Parameter.setMainFolderPath(mainPath);
	}
	
	// Singleton
	// --------------------------------------------------------------------------------------
	
	private static Settings instance;
	
	public static Settings getInstance()
	{
		if(instance == null)
			instance = new Settings();
		
		return instance;
	}

	// Getter & Setter
	// --------------------------------------------------------------------------------------

	public static Properties getAppProps() {
		return appProps;
	}

	public static void setAppProps(Properties appProps) {
		Settings.appProps = appProps;
	}

	public static String getMainPath() {
		return mainPath;
	}

	public static void setMainPath(String mainPath) {
		Settings.mainPath = mainPath;
	}

	public static String getDownloadPath() {
		return downloadPath;
	}

	public static void setDownloadPath(String downloadPath) {
		Settings.downloadPath = downloadPath;
	}
	
	public static int getProcessIssueMilis() {
		return processIssueMilis;
	}

	public static void setProcessIssueMilis(int processIssueMilis) {
		Settings.processIssueMilis = processIssueMilis;
	}

	public static Properties getMessages() {
		return messages;
	}

	public static void setMessages(Properties messages) {
		Settings.messages = messages;
	}
}
