package com.weaseldiesel.common;

public class ApplicationContext 
{
	// Singleton 
	// ---------------------------------------------------------------------------------------
	
	private static ApplicationContext instance;
	
	public static ApplicationContext getInstance()
	{
		if(instance == null)
			instance = new ApplicationContext();
		
		return instance;
	}
	
	// Data Cache
	// ---------------------------------------------------------------------------------------
	
	
	
	// Constructor
	// ---------------------------------------------------------------------------------------
	
	public ApplicationContext()
	{
		
	}
	
	
	// Getter & Setter
	// ---------------------------------------------------------------------------------------
}
