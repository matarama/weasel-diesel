package com.weaseldiesel.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.sql.DataSource;
import javax.naming.InitialContext;
import javax.naming.NamingException;


public class DatabaseConnection 
{
	private static final String user = "postgres";
	private static final String password = "gamespot";
	private static final String connectionUrl = "jdbc:postgresql://127.0.0.1:5432/weasel_diesel";
	
	public static Connection openConnection()
	{
		Connection connection = null;
		try 
		{
			InitialContext cxt = new InitialContext();
			DataSource ds = (DataSource) cxt.lookup("java:/comp/env/jdbc/postgres");
			connection = ds.getConnection();
		} 
		catch (NamingException e) 
		{
			e.printStackTrace();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public static Connection openConnection2()
	{
		Connection connection = null;
		
		try {
			connection = DriverManager.getConnection(connectionUrl, user, password);
		} catch (SQLException e) {
			System.err.println("Cannot connect to database.");
			e.printStackTrace();
		}
		
		return connection;
	}
	
	public static void main(String[] args)
	{
		System.out.println(Issue.getByUserId(1));
	}
}
