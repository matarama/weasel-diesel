package com.weaseldiesel.persistence;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Issue implements Serializable
{
	private static final long serialVersionUID = 6009845104777275166L;
	
	private int userId;
	private int id;
	private String name;
	
	public Issue() 
	{
		super();
	}
	
	public Issue(int userId, int id, String name) 
	{
		super();
		this.userId = userId;
		this.id = id;
		this.name = name;
	}
	
	// Queries
	// ------------------------------------------------------------------
	
	public static List<Issue> getByUserId(int userId)
	{
		List<Issue> issues = new ArrayList<Issue>();
		
		Connection dbCon = DatabaseConnection.openConnection();
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = dbCon.prepareStatement("select * from wd_issue where user_id=?");
			stmt.setInt(1, userId);
			stmt.execute();
			
			rs = stmt.getResultSet();
			
			while(rs.next())
			{
				issues.add(new Issue(userId, rs.getInt("id"), rs.getString("name")));
			}
			
			rs.close();
			stmt.close();
			dbCon.close();
			
		} 
		catch (SQLException e) 
		{
			System.err.println("Problem querying database. (Issue)");
			e.printStackTrace();
		}
		
		return issues;
	}
	
	public static Issue getByUserIdAndName(int userId, String issueName)
	{
		Issue issue = null;
		
		Connection dbCon = DatabaseConnection.openConnection();
		
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try 
		{
			stmt = dbCon.prepareStatement("select * from wd_issue where user_id=? and name=?");
			stmt.setInt(1, userId);
			stmt.setString(2, issueName);
			stmt.execute();
			
			rs = stmt.getResultSet();
			
			if(rs.next())
			{
				issue = new Issue(userId, rs.getInt("id"), rs.getString("name"));
			}
			
			rs.close();
			stmt.close();
			dbCon.close();
			
		} 
		catch (SQLException e) 
		{
			System.err.println("Problem querying database. (Issue)");
			e.printStackTrace();
		}
		
		return issue;
	}
	
	public static boolean createIssue(int userId, String issueName)
	{
		if(getByUserIdAndName(userId, issueName) != null)
			return false;
		
		Connection dbCon = DatabaseConnection.openConnection();
		
		PreparedStatement stmt = null;
		try {
			stmt = dbCon.prepareStatement("insert into wd_issue values(DEFAULT, ?, ?)");
			stmt.setInt(1, userId);
			stmt.setString(2, issueName);
			stmt.executeUpdate();
		} catch (SQLException e) {
			System.err.println("Problem inserting record into database. (Issue)");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	// Getter & Setter
	// ------------------------------------------------------------------

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
