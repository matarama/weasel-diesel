package com.weaseldiesel.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class User 
{
	private int id;
	private String userName;
	private String password;
	private String email;
	
	public User() 
	{
		super();
	}
	
	public User(int id, String userName, String password, String email) 
	{
		super();
		this.id = id;
		this.userName = userName;
		this.password = password;
		this.email = email;
	}

	// Queries
	// ------------------------------------------------------------------------------------
	
	public static User getByUsernameAndPassword(String username, String password)
	{
		User user = null;
		Connection dbCon = DatabaseConnection.openConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try 
		{
			stmt = dbCon.prepareStatement("select * from wd_user where username=? and password=?");
			stmt.setString(1, username);
			stmt.setString(2, password);
			stmt.execute();
			rs = stmt.getResultSet();
			
			if(rs.next())
				user = new User(rs.getInt("id"), username, password, rs.getString("email"));
		} 
		catch (SQLException e) 
		{
			System.err.println("Problem querying database. (User)");
			e.printStackTrace();
		}
		
		return user;
	}
	
	public static User getByUsername(String username)
	{
		User user = null;
		Connection dbCon = DatabaseConnection.openConnection();
		PreparedStatement stmt = null;
		ResultSet rs = null;
		
		try 
		{
			stmt = dbCon.prepareStatement("select * from wd_user where username=?");
			stmt.setString(1, username);
			stmt.execute();
			rs = stmt.getResultSet();
			
			if(rs.next())
				user = new User(rs.getInt("id"), username, rs.getString("password"), rs.getString("email"));
		} 
		catch (SQLException e) 
		{
			System.err.println("Problem querying database. (User)");
			e.printStackTrace();
		}
		
		return user;
	}
	
	public static boolean createUser(String username, String password, String email)
	{
		if(getByUsername(username) != null)
			return false;
		
		Connection dbCon = DatabaseConnection.openConnection();
		PreparedStatement stmt = null;
		
		try 
		{
			stmt = dbCon.prepareStatement("insert into wd_user values (DEFAULT, " + "?, " + "?, " + "?)");
			stmt.setString(1, username);
			stmt.setString(2, email);
			stmt.setString(3, password);
			stmt.executeUpdate();
		} 
		catch (SQLException e) 
		{
			System.err.println("Problem inserting record into database. (User)");
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	// Getter & Setter
	// ------------------------------------------------------------------------------------

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
