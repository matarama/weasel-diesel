package com.weaseldiesel.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.weaseldiesel.common.Settings;
import com.weaseldiesel.persistence.User;

@ManagedBean
@ViewScoped
public class RegisterBean implements Serializable
{
	private static final long serialVersionUID = -447960218631499222L;
	
	private String registerSuccessMessage;
	private String registerFailureMessage;
	private boolean renderSuccessMessage;
	private boolean renderFailureMessage;
	
	private String username;
	private String password;
	private String email;
	
	public RegisterBean()
	{	
		this.username = null;
		this.password = null;
		this.email = null;
		
		this.registerSuccessMessage = Settings.getMessages().getProperty("register-success");
		this.registerFailureMessage = Settings.getMessages().getProperty("register-error-same-username");
		this.renderFailureMessage = false;
		this.renderSuccessMessage = false;
	}

	public void register()
	{
		renderSuccessMessage = User.createUser(username, password, email);
		renderFailureMessage = !renderSuccessMessage;
	}
	
	// Getter & Setter
	// ------------------------------------------------------------------------------------
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public boolean isRenderSuccessMessage() {
		return renderSuccessMessage;
	}

	public void setRenderSuccessMessage(boolean renderSuccessMessage) {
		this.renderSuccessMessage = renderSuccessMessage;
	}

	public boolean isRenderFailureMessage() {
		return renderFailureMessage;
	}

	public void setRenderFailureMessage(boolean renderFailureMessage) {
		this.renderFailureMessage = renderFailureMessage;
	}

	public String getRegisterSuccessMessage() {
		return registerSuccessMessage;
	}

	public void setRegisterSuccessMessage(String registerSuccessMessage) {
		this.registerSuccessMessage = registerSuccessMessage;
	}

	public String getRegisterFailureMessage() {
		return registerFailureMessage;
	}

	public void setRegisterFailureMessage(String registerFailureMessage) {
		this.registerFailureMessage = registerFailureMessage;
	}
}
