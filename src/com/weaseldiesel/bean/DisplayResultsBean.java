package com.weaseldiesel.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.event.NodeSelectEvent;
import org.primefaces.event.SelectEvent;

import com.weaseldiesel.bean.uidata.CbmTableController;
import com.weaseldiesel.bean.uidata.FileTableController;
import com.weaseldiesel.bean.uidata.Pair;
import com.weaseldiesel.bean.uidata.TreeController;
import com.weaseldiesel.common.FacesUtils;
import com.weaseldiesel.common.Settings;
import com.weaseldiesel.common.Utils;
import com.weaseldiesel.core.datastructure.CodeBlockMatch;
import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.datastructure.Line;
import com.weaseldiesel.core.datastructure.StudentMatch;
import com.weaseldiesel.core.main.ProgramDisplay;
import com.weaseldiesel.core.utility.Utility;
import com.weaseldiesel.persistence.Issue;
import com.weaseldiesel.persistence.User;

@ManagedBean
@ViewScoped
public class DisplayResultsBean implements Serializable 
{
	private static final long serialVersionUID = -2536647274448645888L;
	private static final int DEFAULT_ROW_COUNT = 10;
	
	private String manageIssuesMenuItem;
	private String aboutMenuItem;
	private String leftPanelHeader;
	private String fileTableHeader;
	private String codeBlockTableHeader; 
	
	private TreeController treeController;
	private FileTableController fileTableController;
	private CbmTableController cbmTableController;
	private String file1Buffer;
	private String file2Buffer;
	private int codeBlock1RowCount;
	private int codeBlock2RowCount;
	private String codeBlock1;
	private String codeBlock2;
	
	private List<Pair> statistics;
	
	private ProgramDisplay pd;
	private Issue issue;
	
	public DisplayResultsBean()
	{
		User user = (User) FacesUtils.getFromSession(FacesUtils.USER);
		if(user == null)
		{
			FacesUtils.refuseUser();
			return;
		}
		
		// labels
		this.setManageIssuesMenuItem(Settings.getMessages().getProperty("displayresults-label-manage-issues"));
		this.setAboutMenuItem(Settings.getMessages().getProperty("displayresults-label-about"));
		this.setLeftPanelHeader(Settings.getMessages().getProperty("displayresults-label-left-panel-header"));
		this.setFileTableHeader(Settings.getMessages().getProperty("displayresults-label-file-table"));
		this.setCodeBlockTableHeader(Settings.getMessages().getProperty("displayresults-label-code-block-table"));
				
		// fill data
		this.pd = new ProgramDisplay();
		this.issue = (Issue) FacesUtils.getFromSession(FacesUtils.ISSUE);
		
		pd.readCourseReflection(issue.getId() + "");
		this.treeController = new TreeController(pd);
		this.fileTableController = new FileTableController();
		this.cbmTableController = new CbmTableController();
		this.file1Buffer = "";
		this.file2Buffer = "";
		this.codeBlock1 = "";
		this.codeBlock2 = "";
		this.codeBlock1RowCount = DEFAULT_ROW_COUNT;
		this.codeBlock2RowCount = DEFAULT_ROW_COUNT;
		this.statistics = new ArrayList<Pair>();
	}
	
	public void onTreeNodeSelect(NodeSelectEvent event)
	{
		treeController.handleSelection();
		if(treeController.isUpdateChainingComponents())
		{
			StudentMatch match = treeController.getSelectedStudentMatch();
			fileTableController = new FileTableController(match);
/*			
 			statistics = new ArrayList<Pair>();
			statistics.add(new Pair("Normalized Similarity Ratio", "% " + match.getNormalizedSimilarityRatio()));
			statistics.add(new Pair("Signature Similarity Ratio", "% " + match.getSignatureSimilarityRatio()));
			statistics.add(new Pair(match.getStudent1Id() + " Similarity", "% " + match.getStudent1SimilarityRatio()));
			statistics.add(new Pair(match.getStudent2Id() + " Similarity", "% " + match.getStudent2SimilarityRatio()));
*/
			resetCbm();
		}
	}
	
	public void onFileRowSelect(SelectEvent event)
	{
		fileTableController.handleSelection((Pair) event.getObject());
		cbmTableController = new CbmTableController(fileTableController.getSelectedFileMatch());
		
		FileMatch fm = fileTableController.getSelectedFileMatch();
		file1Buffer = Utils.readFile(Utility.findJavaPath(fm.getFile1Path()));
		file2Buffer = Utils.readFile(Utility.findJavaPath(fm.getFile2Path()));
		codeBlock1 = file1Buffer;
		codeBlock2 = file2Buffer;
		codeBlock1RowCount = getRowCount(true);
		codeBlock1RowCount = getRowCount(false);
	}
	
	public void onCbmRowSelect(SelectEvent event)
	{
		cbmTableController.handleSelection((Pair) event.getObject());
		CodeBlockMatch cbm = cbmTableController.getSelectedCodeBlockMatch();
		setHighlightedCodeBlocks(cbm.getFirstFileLines(), cbm.getSecondFileLines());
	}
	
	private void setHighlightedCodeBlocks(Line file1Line, Line file2Line)
	{
		codeBlock1 = highlightString(file1Line.getStartLine(), file1Line.getEndLine(), true).toString();
		codeBlock2 = highlightString(file2Line.getStartLine(), file2Line.getEndLine(), false).toString();
	}
	
	private StringBuffer highlightString(int startLine, int endLine, boolean highlightFile1)
	{
		String[] arr = null;
		if(highlightFile1)
			arr = file1Buffer.split("\n");
		else
			arr = file2Buffer.split("\n");
		
		StringBuffer buffer = new StringBuffer();
		
		int start = startLine - 1;
		int end = endLine - 1;
		for(int i = start; i < end; ++i)
			buffer.append(arr[i] + "\n");
		
		if(highlightFile1)
			codeBlock1RowCount = end - start;
		else
			codeBlock2RowCount = end - start;
		
		return buffer;
	}
	
	public int getRowCount(boolean forFile1)
	{
		if(forFile1)
			return file1Buffer.split("\n").length;
		else
			return file2Buffer.split("\n").length;
	}
	
	private void resetCbm()
	{
		cbmTableController = new CbmTableController();
		codeBlock1 = "";
		codeBlock2 = "";
		codeBlock1RowCount = DEFAULT_ROW_COUNT;
		codeBlock2RowCount = DEFAULT_ROW_COUNT;
		file1Buffer = "";
		file2Buffer = "";
	}
	
	// Getter & Setter
	// ----------------------------------------------------------------------------------------------------
	
	public TreeController getTreeController() {
		return treeController;
	}

	public void setTreeController(TreeController treeController) {
		this.treeController = treeController;
	}

	public FileTableController getFileTableController() {
		return fileTableController;
	}

	public void setFileTableController(FileTableController fileTableController) {
		this.fileTableController = fileTableController;
	}

	public CbmTableController getCbmTableController() {
		return cbmTableController;
	}

	public void setCbmTableController(CbmTableController cbmTableController) {
		this.cbmTableController = cbmTableController;
	}

	public String getFileBuffer1() {
		return file1Buffer;
	}

	public void setFileBuffer1(String fileBuffer1) {
		this.file1Buffer = fileBuffer1;
	}

	public String getFileBuffer2() {
		return file2Buffer;
	}

	public void setFileBuffer2(String fileBuffer2) {
		this.file2Buffer = fileBuffer2;
	}

	public String getCodeBlock1() {
		return codeBlock1;
	}

	public void setCodeBlock1(String codeBlock1) {
		this.codeBlock1 = codeBlock1;
	}

	public String getCodeBlock2() {
		return codeBlock2;
	}

	public void setCodeBlock2(String codeBlock2) {
		this.codeBlock2 = codeBlock2;
	}

	public int getCodeBlock1RowCount() {
		return codeBlock1RowCount;
	}

	public void setCodeBlock1RowCount(int codeBlock1RowCount) {
		this.codeBlock1RowCount = codeBlock1RowCount;
	}

	public int getCodeBlock2RowCount() {
		return codeBlock2RowCount;
	}

	public void setCodeBlock2RowCount(int codeBlock2RowCount) {
		this.codeBlock2RowCount = codeBlock2RowCount;
	}

	public List<Pair> getStatistics() {
		return statistics;
	}

	public void setStatistics(List<Pair> statistics) {
		this.statistics = statistics;
	}

	public String getManageIssuesMenuItem() {
		return manageIssuesMenuItem;
	}
	
	public void setManageIssuesMenuItem(String manageIssuesMenuItem) {
		this.manageIssuesMenuItem = manageIssuesMenuItem;
	}

	public String getAboutMenuItem() {
		return aboutMenuItem;
	}

	public void setAboutMenuItem(String aboutMenuItem) {
		this.aboutMenuItem = aboutMenuItem;
	}

	public String getLeftPanelHeader() {
		return leftPanelHeader;
	}

	public void setLeftPanelHeader(String leftPanelHeader) {
		this.leftPanelHeader = leftPanelHeader;
	}

	public String getFileTableHeader() {
		return fileTableHeader;
	}

	public void setFileTableHeader(String fileTableHeader) {
		this.fileTableHeader = fileTableHeader;
	}

	public String getCodeBlockTableHeader() {
		return codeBlockTableHeader;
	}

	public void setCodeBlockTableHeader(String codeBlockTableHeader) {
		this.codeBlockTableHeader = codeBlockTableHeader;
	}
	
}
