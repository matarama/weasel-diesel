package com.weaseldiesel.bean;

import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.event.ActionListener;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.weaseldiesel.common.FacesUtils;
import com.weaseldiesel.common.Settings;
import com.weaseldiesel.core.main.Parameter;
import com.weaseldiesel.core.utility.Keyword;
import com.weaseldiesel.persistence.Issue;
import com.weaseldiesel.persistence.User;
import com.weaseldiesel.thread.HandleIssueJob;

@ManagedBean
@ViewScoped
public class ManageIssuesBean implements Serializable
{
	private static final long serialVersionUID = 2110794751708027261L;
	
	private String successMessage;
	private String errorMessage;
	private String errorMessage2;
	private String errorMessage3;
	private String labelNewIssue;
	private String labelIssueName;
	private String labelPreviousIssues;
	private String labelId;
	private String labelName;
	private String labelDisplayResults;
	private String labelUploadFile;
	private String labelFileFormat;
	private String labelCreateIssue;
	
	private boolean renderSuccessMessage;
	private boolean renderFailureMessage;
	private UploadedFile uploadedFile;
	
	private int userId;
	private List<Issue> issues;
	private Issue selectedIssue;
	private String issueName;
	
	public ManageIssuesBean()
	{
		User user = (User) FacesUtils.getFromSession(FacesUtils.USER);
		if(user == null)
		{
			FacesUtils.refuseUser();
			return;
		}
		
		this.errorMessage = Settings.getMessages().getProperty("manageissue-error-existing-issue-name");
		this.successMessage = Settings.getMessages().getProperty("manageissue-success-create-new-issue");
		this.errorMessage2 = Settings.getMessages().getProperty("manageissue-error-missing-upload-file");
		this.errorMessage3 = Settings.getMessages().getProperty("manageissue-error-missing-issue-name");
		this.labelNewIssue = Settings.getMessages().getProperty("manageissue-label-new-issue");
		this.labelIssueName = Settings.getMessages().getProperty("manageissue-label-issue-name");
		this.labelPreviousIssues = Settings.getMessages().getProperty("manageissue-label-previous-issues");
		this.labelId = Settings.getMessages().getProperty("manageissue-label-id");
		this.labelName = Settings.getMessages().getProperty("manageissue-label-name");
		this.labelDisplayResults = Settings.getMessages().getProperty("manageissue-label-display-results");
		this.labelUploadFile = Settings.getMessages().getProperty("manageissue-label-upload-file");
		this.labelFileFormat = Settings.getMessages().getProperty("manageissue-label-file-format");
		this.labelCreateIssue = Settings.getMessages().getProperty("manageissue-label-create-issue");
		
		this.renderFailureMessage = false;
		this.renderSuccessMessage = false;
		
		this.userId = user.getId();
		this.issues = Issue.getByUserId(userId);
		this.selectedIssue = null;
		this.issueName = null;
		this.uploadedFile = null;
		
	}
	
	public String selectIssue()
	{
		if(selectedIssue == null)
			return "no_action";
		
		FacesUtils.putToSession(FacesUtils.ISSUE, selectedIssue);
		return "select_issue";
	}
	
	public void createIssue()
	{
		renderSuccessMessage = Issue.createIssue(userId, issueName);
		renderFailureMessage = !renderSuccessMessage;
		
		if(renderSuccessMessage)
		{
			Issue issue = Issue.getByUserIdAndName(userId, issueName);
			String fileName = issue.getId() + "";
			copyFileToDownloadFolder(uploadedFile, fileName + Keyword.ZIP_EXTENSION);
			
			// add issue to schedulers issue process queue
	        new HandleIssueJob().addIssue(issue);
		}
	}
	
	public void loadFileFormatTree(ActionEvent evt)
	{
		System.out.println("Action Event");
	}
	
	public void loadFileFormatTree(ActionListener evt)
	{
		System.out.println("Action Listener");
	}
	
	public void handleFileUpload(FileUploadEvent evt)
	{
		uploadedFile = evt.getFile();
	}

    private void copyFileToDownloadFolder(UploadedFile uploadedFile, String fileName)
    {
    	String uploadedFilePath = Parameter.getDownloadFolderPath() + "\\" + fileName;
        InputStream is = null;
        OutputStream os = null;
        byte[] buffer = new byte[2048];
        try {
            is = uploadedFile.getInputstream();
            os = new FileOutputStream(uploadedFilePath);

            int len;
            while ((len = is.read(buffer)) > 0) {
                os.write(buffer, 0, len);
            }

            is.close();
            os.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ManageIssuesBean.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ManageIssuesBean.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
	
	// Getter & Setter
	// -----------------------------------------------------------------------------------------
	
	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	public Issue getSelectedIssue() {
		return selectedIssue;
	}

	public void setSelectedIssue(Issue selectedIssue) {
		this.selectedIssue = selectedIssue;
	}

	public String getIssueName() {
		return issueName;
	}

	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}

	public boolean isRenderSuccessMessage() {
		return renderSuccessMessage;
	}

	public void setRenderSuccessMessage(boolean renderSuccessMessage) {
		this.renderSuccessMessage = renderSuccessMessage;
	}

	public boolean isRenderFailureMessage() {
		return renderFailureMessage;
	}

	public void setRenderFailureMessage(boolean renderFailureMessage) {
		this.renderFailureMessage = renderFailureMessage;
	}

	public String getSuccessMessage() {
		return successMessage;
	}

	public void setSuccessMessage(String successMessage) {
		this.successMessage = successMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getErrorMessage2() {
		return errorMessage2;
	}

	public void setErrorMessage2(String errorMessage2) {
		this.errorMessage2 = errorMessage2;
	}

	public String getLabelIssueName() {
		return labelIssueName;
	}

	public void setLabelIssueName(String labelIssueName) {
		this.labelIssueName = labelIssueName;
	}

	public String getLabelPreviousIssues() {
		return labelPreviousIssues;
	}

	public void setLabelPreviousIssues(String labelPreviousIssues) {
		this.labelPreviousIssues = labelPreviousIssues;
	}

	public String getLabelId() {
		return labelId;
	}

	public void setLabelId(String labelId) {
		this.labelId = labelId;
	}

	public String getLabelName() {
		return labelName;
	}

	public void setLabelName(String labelName) {
		this.labelName = labelName;
	}

	public String getLabelDisplayResults() {
		return labelDisplayResults;
	}

	public void setLabelDisplayResults(String labelDisplayResults) {
		this.labelDisplayResults = labelDisplayResults;
	}

	public String getLabelUploadFile() {
		return labelUploadFile;
	}

	public void setLabelUploadFile(String labelUploadFile) {
		this.labelUploadFile = labelUploadFile;
	}

	public String getLabelFileFormat() {
		return labelFileFormat;
	}

	public void setLabelFileFormat(String labelFileFormat) {
		this.labelFileFormat = labelFileFormat;
	}

	public String getErrorMessage3() {
		return errorMessage3;
	}

	public void setErrorMessage3(String errorMessage3) {
		this.errorMessage3 = errorMessage3;
	}

	public String getLabelNewIssue() {
		return labelNewIssue;
	}

	public void setLabelNewIssue(String labelNewIssue) {
		this.labelNewIssue = labelNewIssue;
	}

	public String getLabelCreateIssue() {
		return labelCreateIssue;
	}

	public void setLabelCreateIssue(String labelCreateIssue) {
		this.labelCreateIssue = labelCreateIssue;
	}
}
