package com.weaseldiesel.bean;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.weaseldiesel.common.FacesUtils;
import com.weaseldiesel.common.Settings;
import com.weaseldiesel.persistence.User;


@ManagedBean
@ViewScoped
public class LoginBean implements Serializable
{
	private static final long serialVersionUID = 2690738463079234228L;
	
	private String failureMessage;
	private boolean renderFailureMessage;
	
	private String username;
	private String password;
	
	public LoginBean()
	{
		this.username = null;
		this.password = null;
		
		this.failureMessage = Settings.getMessages().getProperty("login-error");
		this.renderFailureMessage = false;
	}

	public String login()
	{
		User user = User.getByUsernameAndPassword(username, password);
		if(user == null)
		{
			renderFailureMessage = true;
			return "failure";
		}
		
		FacesUtils.putToSession(FacesUtils.USER, user);
		return "success";
	}
	
	public String register()
	{
		return "";
	}
	
	public String emailPassword()
	{
		return "";
	}
	
	// Getter & Setter
	// ------------------------------------------------------------------------------------
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFailureMessage() {
		return failureMessage;
	}

	public void setFailureMessage(String failureMessage) {
		this.failureMessage = failureMessage;
	}

	public boolean isRenderFailureMessage() {
		return renderFailureMessage;
	}

	public void setRenderFailureMessage(boolean renderFailureMessage) {
		this.renderFailureMessage = renderFailureMessage;
	}
}
