package com.weaseldiesel.bean;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

import com.weaseldiesel.common.Settings;

public class MenuBean implements Serializable
{
	private static final long serialVersionUID = -7337867620926356810L;
	
	private StreamedContent userManual;
	
	public MenuBean()
	{
		File file = new File(Settings.USER_MANUAL_PATH);
		
		InputStream stream = null;
		try {
			stream = new FileInputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		userManual = new DefaultStreamedContent(stream, "pdf", "Weasel-Deisel_user_manual.pdf");
	}
	
	// Navigation
	// ---------------------------------------------------------------------------------------------
	
	public String goToAbout()
	{
		return "about_weasel_diesel";
	}
	
	public String createNewIssue()
	{
		return "new_issue";
	}
	
	public String displayPreviousIssues()
	{
		return "display_issues";
	}
	
	// Getter & Setter
	// ---------------------------------------------------------------------------------------------
	
	public StreamedContent getUserManual() {
		return userManual;
	}

	public void setUserManual(StreamedContent userManual) {
		this.userManual = userManual;
	}
}
