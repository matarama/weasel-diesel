package com.weaseldiesel.bean.uidata;

import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

public class PairDataModel extends ListDataModel<Pair> implements SelectableDataModel<Pair>
{
	public Pair selection;
	
	// Constructors
	// -----------------------------------------------------------------------------
	
	public PairDataModel() 
	{
		super();
	}

	public PairDataModel(List<Pair> dataList)
	{
		super(dataList);
	}
	
	// Methods
	// -----------------------------------------------------------------------------
	
	@Override
	@SuppressWarnings("unchecked")
	public Pair getRowData(String rowKey) 
	{
		List<Pair> pairs = (List<Pair>) getWrappedData();
		String rk = rowKey.substring(8);
		for(Pair p : pairs)
			if(p.getData1().equals(rk))
			{
				selection = p;
				return p;
			}
				
				
		return null;
	}

	@Override
	public String getRowKey(Pair pair) 
	{
		return "row_key:" + pair.getData1();
	}

}
