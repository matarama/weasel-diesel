package com.weaseldiesel.bean.uidata;

import com.weaseldiesel.core.datastructure.StudentMatch;

public class StudentNodeData 
{
	private StudentMatch match;
	private String renderedText;
	private boolean isMatchNode;
	
	// Constructors
	// ------------------------------------------------------------------------------------------
	
	public StudentNodeData()
	{
	}
	
	public StudentNodeData(String renderedText) 
	{
		super();
		this.match = null;
		this.renderedText = renderedText;
		this.isMatchNode = false;
	}

	public StudentNodeData(StudentMatch match, String studentId) 
	{
		super();
		this.match = match;
		this.renderedText = studentId + " (%" + match.getNormalizedSimilarityRatio() + ")";
		this.isMatchNode = true;
	}
	
	// Methods
	// ----------------------------------------------------------------------------------------------

	public String toString()
	{
		return this.getRenderedText();
	}
	
	// Getter & Setter
	// ---------------------------------------------------------------------------
	
	public StudentMatch getMatch() {
		return match;
	}
	
	public void setMatch(StudentMatch match) {
		this.match = match;
	}
	
	public String getRenderedText() {
		return renderedText;
	}

	public void setRenderedText(String renderedText) {
		this.renderedText = renderedText;
	}

	public boolean isMatchNode() {
		return isMatchNode;
	}

	public void setMatchNode(boolean isMatchNode) {
		this.isMatchNode = isMatchNode;
	}
}
