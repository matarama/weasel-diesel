package com.weaseldiesel.bean.uidata;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.TreeNode;

import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.datastructure.StudentMatch;
import com.weaseldiesel.core.main.ProgramDisplay;
import com.weaseldiesel.core.reflection.Student;

// UI Tree Component's Controller (DisplayResultsBean.java - displayresults.xhtml)
// ----------------------------------------------------------------------------------------	

public class TreeController
{
	private TreeNode root;
	private TreeNode selectedNode;
	private StudentNodeData selectedStudentMatchNodeData;
	private StudentMatch selectedStudentMatch;
	private List<FileMatch> selectedFileMatchList;
	private boolean updateChainingComponents;
	
	public TreeController(ProgramDisplay pd)
	{
		this.root = new DefaultTreeNode();
		Map<String, Integer> studentMatchCountMap = pd.getStudentMatchCountMap();
		
		for(Entry<String, Integer> e : studentMatchCountMap.entrySet())
		{
			String student1Id = e.getKey();
			int student1MatchCount = e.getValue();
			
			DefaultTreeNode parentNode = new DefaultTreeNode(new StudentNodeData(student1Id), root);
			parentNode.setExpanded(true);
			
			Student student1 = pd.findStudent(student1Id);
			for(int i = 0; i < student1MatchCount; ++i)
			{
				String student2Id = pd.getMatchName(student1, i);
				StudentMatch match = pd.readStudentMatch(student1Id, student2Id);
				new DefaultTreeNode(new StudentNodeData(match, student2Id), parentNode);
			}
		}
	}
	
	public void handleSelection() 
	{
		setSelectedStudentMatchNodeData((StudentNodeData) getSelectedNode().getData());
		setSelectedStudentMatch(getSelectedStudentMatchNodeData().getMatch());
		
		if(getSelectedStudentMatchNodeData().isMatchNode())
		{
			setSelectedFileMatchList(getSelectedStudentMatch().getFileMatches());
			setUpdateChainingComponents(true);
		}
		else
			setUpdateChainingComponents(false);
	}
	
	// Getter & Setter
	// -------------------------------------------------------------------------------------

	public TreeNode getRoot() {
		return root;
	}

	public void setRoot(TreeNode root) {
		this.root = root;
	}

	public TreeNode getSelectedNode() {
		return selectedNode;
	}

	public void setSelectedNode(TreeNode selectedNode) {
		this.selectedNode = selectedNode;
	}

	public StudentMatch getSelectedStudentMatch() {
		return selectedStudentMatch;
	}

	public void setSelectedStudentMatch(StudentMatch selectedStudentMatch) {
		this.selectedStudentMatch = selectedStudentMatch;
	}

	public List<FileMatch> getSelectedFileMatchList() {
		return selectedFileMatchList;
	}

	public void setSelectedFileMatchList(List<FileMatch> selectedFileMatchList) {
		this.selectedFileMatchList = selectedFileMatchList;
	}

	public StudentNodeData getSelectedStudentMatchNodeData() {
		return selectedStudentMatchNodeData;
	}

	public void setSelectedStudentMatchNodeData(
			StudentNodeData selectedStudentMatchNodeData) {
		this.selectedStudentMatchNodeData = selectedStudentMatchNodeData;
	}

	public boolean isUpdateChainingComponents() {
		return updateChainingComponents;
	}

	public void setUpdateChainingComponents(boolean updateChainingComponents) {
		this.updateChainingComponents = updateChainingComponents;
	}
}
