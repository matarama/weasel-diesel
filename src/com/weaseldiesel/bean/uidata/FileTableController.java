package com.weaseldiesel.bean.uidata;

import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.datastructure.StudentMatch;
import com.weaseldiesel.core.utility.Utility;

public class FileTableController 
{
	private List<FileMatch> fileMatchList;
	private Pair selectedPair;
	private FileMatch selectedFileMatch;
	private PairDataModel dataModel;
	private String column1Name;
	private String column2Name;
	
	// Constructors
	// ----------------------------------------------------------------------------------------------------
	
	public FileTableController()
	{
		super();
		this.dataModel = new PairDataModel();
	}

	public FileTableController(StudentMatch match)
	{
		super();
		this.column1Name = match.getStudent1Id();
		this.column2Name = match.getStudent2Id();
		this.fileMatchList = match.getFileMatches();
		
		List<Pair> pairList = new ArrayList<Pair>();
		for(FileMatch fm : fileMatchList)
		{
			String displayText1 = Utility.getJavaFromSer(fm.getFile1ID()) + " [%" + fm.getFile1SimilarityRatio() + "]";
			String displayText2 = Utility.getJavaFromSer(fm.getFile2ID()) + " [%" + fm.getFile2SimilarityRatio() + "]";
			pairList.add(new Pair(fm.getFile1ID(), fm.getFile2ID(), displayText1, displayText2));
		}
			
		
		this.dataModel = new PairDataModel(pairList);
	}
	
	// Methods
	// ----------------------------------------------------------------------------------------------------
	
	public void handleSelection(Pair pair)
	{
		this.selectedPair = pair;
		this.selectedFileMatch = findFileMatch(pair);
	}
	
	private FileMatch findFileMatch(Pair pair)
	{
		for(FileMatch fm : fileMatchList)
			if((fm.getFile1ID().equals(pair.getData1()) && fm.getFile2ID().equals(pair.getData2())) || 
				(fm.getFile1ID().equals(pair.getData2()) && fm.getFile2ID().equals(pair.getData1())))
				return fm;
				
		return null;
	}
	
	// Getter & Setter
	// --------------------------------------------------------------------------

	public PairDataModel getDataModel() {
		return dataModel;
	}

	public List<FileMatch> getFileMatchList() {
		return fileMatchList;
	}

	public void setFileMatchList(List<FileMatch> fileMatchList) {
		this.fileMatchList = fileMatchList;
	}

	public void setDataModel(PairDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public void setSelectedFileMatch(FileMatch selectedFileMatch) {
		this.selectedFileMatch = selectedFileMatch;
	}

	public FileMatch getSelectedFileMatch() {
		return selectedFileMatch;
	}
	
	public Pair getSelectedPair() {
		return selectedPair;
	}

	public void setSelectedPair(Pair selectedPair) {
		this.selectedPair = selectedPair;
	}

	public String getColumn1Name() {
		return column1Name;
	}

	public void setColumn1Name(String column1Name) {
		this.column1Name = column1Name;
	}

	public String getColumn2Name() {
		return column2Name;
	}

	public void setColumn2Name(String column2Name) {
		this.column2Name = column2Name;
	}
}
