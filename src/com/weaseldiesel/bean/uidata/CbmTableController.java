package com.weaseldiesel.bean.uidata;

import java.util.ArrayList;
import java.util.List;

import com.weaseldiesel.core.datastructure.CodeBlockMatch;
import com.weaseldiesel.core.datastructure.FileMatch;
import com.weaseldiesel.core.utility.Utility;

public class CbmTableController 
{
	private List<CodeBlockMatch> cbmList;
	private Pair selectedPair;
	private CodeBlockMatch selectedCodeBlockMatch;
	private PairDataModel dataModel;
	private String column1Name;
	private String column2Name;
	
	// Constructors
	// -----------------------------------------------------------------------------------------------------
	
	public CbmTableController()
	{
		super();
		this.dataModel = new PairDataModel();
		this.column1Name = "File 1";
		this.column2Name = "File 2";
	}
	
	public CbmTableController(FileMatch fileMatch)
	{
		this.column1Name = Utility.getJavaFromSer(fileMatch.getFile1ID());
		this.column2Name = Utility.getJavaFromSer(fileMatch.getFile2ID());
		
		this.cbmList = fileMatch.getCodeBlockMatches();
		List<Pair> pairList = new ArrayList<Pair>();
		for(CodeBlockMatch cbm : cbmList)
			pairList.add(new Pair(cbm.getFirstFileLines().toString(),
								cbm.getSecondFileLines().toString()));
				
		dataModel = new PairDataModel(pairList);
	}
	
	// Methods
	// -----------------------------------------------------------------------------------------------------
	
	public void handleSelection(Pair pair)
	{
		this.selectedPair = pair;
		this.selectedCodeBlockMatch = findCbm(pair);
	}
	
	private CodeBlockMatch findCbm(Pair pair)
	{
		for(CodeBlockMatch cbm : cbmList)
			if((cbm.getFirstFileLines().toString().equals(pair.getData1()) && cbm.getSecondFileLines().toString().equals(pair.getData2())) ||
				(cbm.getFirstFileLines().toString().equals(pair.getData2()) && cbm.getSecondFileLines().toString().equals(pair.getData1())))
				return cbm;
		
		return null;
	}
	
	// Getter & Setter
	// ---------------------------------------------------------------------------------------------------
	
	public PairDataModel getDataModel() {
		return dataModel;
	}
	
	public List<CodeBlockMatch> getCbmList() {
		return cbmList;
	}

	public void setCbmList(List<CodeBlockMatch> cbmList) {
		this.cbmList = cbmList;
	}

	public void setDataModel(PairDataModel dataModel) {
		this.dataModel = dataModel;
	}

	public Pair getSelectedPair() {
		return selectedPair;
	}

	public void setSelectedPair(Pair selectedPair) {
		this.selectedPair = selectedPair;
	}

	public CodeBlockMatch getSelectedCodeBlockMatch() {
		return selectedCodeBlockMatch;
	}

	public void setSelectedCodeBlockMatch(CodeBlockMatch selectedCodeBlockMatch) {
		this.selectedCodeBlockMatch = selectedCodeBlockMatch;
	}

	public String getColumn1Name() {
		return column1Name;
	}

	public void setColumn1Name(String column1Name) {
		this.column1Name = column1Name;
	}

	public String getColumn2Name() {
		return column2Name;
	}

	public void setColumn2Name(String column2Name) {
		this.column2Name = column2Name;
	}
}
