package com.weaseldiesel.bean.uidata;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Pair
{
	private String data1;
	private String data2;
	
	private String displayedText1;
	private String displayedText2;
	
	// Constructors
	// -----------------------------------------------------------------------------
	
	public Pair(String data1, String data2)
	{
		this.data1 = data1;
		this.data2 = data2;
		this.displayedText1 = data1;
		this.displayedText2 = data2;
	}
	
	public Pair(String data1, String data2, String displayedText1,
			String displayedText2) 
	{
		super();
		this.data1 = data1;
		this.data2 = data2;
		this.displayedText1 = displayedText1;
		this.displayedText2 = displayedText2;
	}

	// Methods
	// -----------------------------------------------------------------------------
	
	public static List<Pair> getAsPairList(Map<String, Integer> map)
	{
		List<Pair> list = new ArrayList<Pair>();
		
		for(Entry<String, Integer> e : map.entrySet())
			list.add(new Pair(e.getKey().toString(), e.getValue().toString()));
		
		return list;
	}
	
	public String toString()
	{
		return "[Pair: (" + data1 + ", " + data2 + ")]";
	}
	
	// Getter & Setter
	// -----------------------------------------------------------------------------

	public String getData1() {
		return data1;
	}

	public void setData1(String data1) {
		this.data1 = data1;
	}

	public String getData2() {
		return data2;
	}

	public void setData2(String data2) {
		this.data2 = data2;
	}
	
	public String getDisplayedText1() {
		return displayedText1;
	}

	public void setDisplayedText1(String displayedText1) {
		this.displayedText1 = displayedText1;
	}

	public String getDisplayedText2() {
		return displayedText2;
	}

	public void setDisplayedText2(String displayedText2) {
		this.displayedText2 = displayedText2;
	}
}